<?php # $Id: ViewViews.cl.php,v 1.5 2004-06-10 02:00:38 paulmcav Exp $

IncludeObject('.','db_mysql');

/** ViewViews page
*
*/
class ViewViews extends Smarty
{
	var $name = "ViewViews";
	var $title = "View Exif: ";
	var $db;
	var $config;

	function ViewViews()
	{
		global $session; 
	
		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		if ( $session['userid'] == '' ){ return; }

		$_ID = $_REQUEST['id'];
		if ( $_ID != '' ) {
//			echo "id: $_ID<br>"; print_r( $ex );
			$ex = explode( '|', $_ID );
			$_exif['dbid'] = $ex[0];
			$_exif['path'] = $ex[1];
			$_exif['name'] = $ex[2];
		}
		else {
			$_exif = $session['exif_data'];
		}
		$base = substr($_exif['name'],0,-4);
		$path = strstr( $_exif['path'], '/' );
		
		$_view_data = $this->do_get_view_data( $this->db, $_exif['dbid'] );
		
//		echo "<pre>"; print_r( $ex_data ); echo "</pre>";
		
		$this->assign( array(
			'page_title' => $this->title.$base,
			'view_data'  => $_view_data,
			'base_name'  => $base,
			'base_path'  => $path,
			)
	   	);

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------

	function do_get_view_data( $db, $_DBID )
	{
		$sql = "SELECT *,DATE_FORMAT(iv.ts,'%d%b%y %H:%i') nicedate"
			." FROM image_view iv"
			." LEFT JOIN user u on u.id=iv.user_id"
			." WHERE image_id=".$_DBID
			." ORDER BY iv.id"
			;

		$db->query( $sql );

		while( $db->next_record() ) {
			$row = $db->Record;
//			echo "<pre>"; print_r( $row ); echo "</pre>";

			$views[] = array(
				'email' => $row['email'],
				'res'   => $row['res'],
				'date'  => $row['nicedate'],
				'addr'  => $row['remote_addr'],
			);
		}
		return $views;
	}
}

