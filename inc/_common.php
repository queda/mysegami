<?php # $Id: _common.php,v 1.9 2004-05-28 02:21:50 paulmcav Exp $

	# common support functions
	
// generic page setup stuff
function PageSetup( $cfg, $obj )
{
	global $session;

	// -- check to see if we want a connection to the db
	if ( get_class( $obj->db ) == "db" ) {
		$obj->config_load( $cfg,'db' );
		$dbv = $obj->get_config_vars( );
		$obj->config = $dbv;
		
//		echo "<pre>"; print_r( $dbv ); echo "</pre>";
		
		$obj->db->connect(
			$dbv['database'], $dbv['host'], $dbv['user'], $dbv['password'] );
	}	
	
	// found a userid cookie, go get name if not set
	if ($HTTP_COOKIE_VARS['userid'] >0 && $session['userem'] == '') {
		ValidateUser( $obj->db, $dbv, $HTTP_COOKIE_VARS['userid'], -1 );
	}

	if ( $session['userem'] == '' ) {
		$_logout_url = "";
		$_login_url = '<a href="?'.enc64("page=Login").'">Login</a> / '
					.'<a href="?'.enc64("page=Register").'">Register</a>';
	}
	else {
//		echo "userid: '".$session['userem']."'<br>";
		$_logout_url = '<a href="?'.enc64("page=Login&cmd=Logout")
						.'">Logout</a> |';
		$_login_url = '<a href="?'.enc64("page=Profile").'">'
			.$session['userem'].'</a>';
		$_login_url = $session['userem'];
	}
	
	$obj->assign( array(
		'login_url'  => $_login_url,
   		'logout_url' => $_logout_url,
	) );
	
	// -- determine which site user is coming from/to
//	if ($session['srv_nm'] != $_SERVER['SERVER_NAME']) {
//		LocateSite( $_SERVER['SERVER_NAME'] );
//	}

	
}

// determine if we are being called via Alias Match, or a ?page=<request>
//
function PageRequest( $aliasurl, $page, $tpl="" )
{
	global $HTTP_SERVER_VARS;

	if ( ! $page ) {
		$script = $HTTP_SERVER_VARS['SCRIPT_URL'];
		$sub = substr($script,0,strlen($aliasurl));

		// does SCRIPT_URL match base of 'alias_url'?, take remaining part
		if ( $sub == $aliasurl) {
			if (substr($sub,-1) != "/"){ $sub .= "/"; }

			$page = substr($script,strlen($sub));
			// has .html extension, handle it.
			if ($pos = strpos($page,".html")) {
#	echo "have '.'<br>";
				$page = substr($page,0,$pos);
			}
			// not .html, but has some extension, passthru
			else if (strpos($page,".")) {
				if ( file_exists( $tpl.$page ) ) {
					Header( "Content-type: ".mime_content_type( $tpl.$page ));
					readfile( $tpl.$page ); die;
				}
				else {
					error404( "$tpl$page" );
				}
#	echo "some image: $tpl$page<br"; die;
			}
			// assume it should be .html
			else {
#	echo "no .html<br>";
				$page .= "Index";
			}
#	echo "SCRIPT_URL: ".$script. "<br>";
#	echo "sub: $sub, aliasurl: ".$globs['aliasurl']."<br>";
#	echo "Page: $page<br>";
		}
	}
	return $page;
}

if (!function_exists ("mime_content_type")) {
 function mime_content_type ($file) {
  return exec ("file -bi $file");
 }
}

function error404( $page )
{
	@header("HTTP/1.0 404 Not Found");
	echo "<p><h2>Error 404</h2>File not found: ";
	echo "<i><font color=red>$page</font></i><br>";
}


# super generic class to display an html page.
class HtmlPage extends Smarty
{
	var $data;

	function HtmlPage( $file ) {
		$fdata = @file("templates/".$file.".html");
		if (!$fdata ) {
			$fdata = @file("templates/404.html");
			if (!$fdata) {
				error404( "$file.html" );
				die;
			}
		}
		$this->data = implode("",$fdata);
	}

	function main() {
		global $globs;

		if ($globs['filtpage'] == TRUE) {
			print filter_href_fix( $this->data );
		}
		else {
			print $this->data ;
		}
	}
}

function IncludeObject($root, $obj)
{
	$path = '';
	if ( !($class = substr(strrchr($obj, '/'),1)) ) {
		$class = $obj;
	}
	else {
		$path = '/'.substr($obj,0,strrpos($obj,'/'));
	}
#		$path .= "/inc";
	$path = "/inc".$path;
	
	if ( class_exists($class) ) {
		return 0;
	}
	
	$file = $root.$path.'/'.$class.'.cl.php';

	if ( file_exists($file) ) {
		include($file);
		return $class;
	}
	return '';
}

function CreateObject($root, $obj, $parms='_F_' )
{
	$class = IncludeObject($root,$obj);

	# if no class was found, try a generic html page
	if ( $class=='') {
		$class = "HtmlPage";
#			$class = IncludeObject("Page");
		$parms = array($obj);
	}

	$code = '$ret = new '.$class.'(';
	
	for ($i=0;$parms != '_F_' && $i<count($parms);$i++) {
		$code .= ($i ? ',':'')."'".$parms[$i]."'";
	}

	$code .= ');';
	eval( $code );
	return $ret;
}

/**
*
* 0  = found user, password match
* -1 = !found user
* -2 = found user, !password match
*/
function FindUser( $db, $email, $passwd='', &$row )
{
	if ( $email > 0 ) {
		$field = "id";
	}
	else {
		$field = "email";
	}
	
	$sql = "SELECT id,email,passwd,name FROM user WHERE"
		." $field='$email'";
	   
//	if ( $passwd != '' ) { $sql .= " AND passwd='$passwd'"; }

	$db->query( $sql );
	if ( $db->next_record() ) {
		$row = $db->Record;

		if ( $row['passwd'] != $passwd ){ 
			return -2;
	   	}
		else {
			return 0;
		}
	}
	else {
		return -1;
	}
}


function ValidateUser( $db,$cfgv, $email, $pass )
{
	global $session;

	// check if logging in as the admin
	/*
	if ( $email == 'admin' && $pass == $cfgv['adminpw'] ) {
		$session['loginok'] = -1;
		$session['userid'] = -1;
		$session['userem'] = 'admin';
		setcookie( 'userid', -1, 0, "/" );

		return -1;
	}
	*/

	$ret = FindUser( $db, $email, $pass, $row );

	// Invalid user or password given
	if ($ret) {
		$email = '';		

		unset( $session['userid'] );
		unset( $session['userem'] );
		setcookie( 'userid', '', time()-2678400, "/" );

		return $ret;
	}

	// valid user found, set info
	$user  = $row['id'];
	$parts = explode( '@',$row['email'] );
	$name  = $parts[0];

	// Set username if empty
	if ( $row['name'] == '' ) {
		$row['name'] = $name;
		$db->query( "UPDATE user set name='$name' WHERE id=$user" );
	}
	
	$session['loginok'] = $user;
	$session['userid']  = $user;
	$session['userem']  = $row['name'];

	setcookie( 'userid', $user, time()+2678400, "/" );

	// update visit count
	$db->query( "UPDATE user set visits=visits+1 WHERE id=$user" );
	
//echo "user: $user<br>";
	return $user;
}

function gen_http_get( $vars='' )
{
	parse_str($vars,$rq_data);
	$rq_data = array_merge($_GET,$rq_data);

	while( list($k,$v)=each($rq_data) ) {
		$buff .= ($buff!='' ? '&':'')."$k=$v";
	}
	return $buff;
}

function slashes( $str, $n=1 )
{
	if ( !$n || ini_get('magic_quotes_gpc')==1
		|| ini_get('magic_quotes_runtime')==1
	   ) {
		return $str;
	}
	else {
		return addslashes( $str );
	}
}

function form2query( $form_data, $f_unlink=0, $req='' )
{
	$sql = '';
	
	while (list($k,$v)=each($form_data)) {
		$t = substr($k,0,1);
		$n = substr($k,2);

		// field has data: [ ?_<fld> : n=#, (s)tr, (d)ate, (t)ime ]
		if ( $v!='' ) {
			if ( $t=='s' || $t=='f' ){ $v = slashes($v); }
			else if ( $t=='d' ){ $v = date("Y-m-d",strtotime($v)); }
			else if ( $t=='t' ){ $v = date("Y-m-d H:i:s",strtotime($v)); }

			if ( $t!='_' ){
				$sql .= ",$n=".($t=='n' ? "$v" : "'$v'");
			}
		}
	}
	return $sql;
}

function move_upload_file( $file, $time=1 )
{
	$fname = '';
	if ( is_uploaded_file($file['tmp_name']) ) {
		if ($time==1) {
			$fname = filectime($file['tmp_name']).'-'.$file['name'];
		}
		else {
			$fname = $file['name'];
		}
		copy($file['tmp_name'], "data/$fname" );
	}
	else {
		// not an uploaded file! ?? attack underway ??
	}
	return $fname;
}
	
function filter_href_fix( $str )
{
	$reg = "/(href\\s*=\\s*)\"([^.?=]\\w*)(\.html)[^?]?\"/sim";
	$rep = "\$1\"?page=\$2\"";

	return preg_replace( $reg , $rep , $str , -1 );
}

function _initials($name)
{
	$names = explode(" ",$name);
	$_name = "";
	for( $i=0; $i<count($names); $i++ ) {
		$_name .= $names[$i][0];
	}
	return $_name;
}

// base64 encode function
function enc64( $str ){
	if ( ENCODE_64 ) { return base64_encode($str); } else 
	{ return $str; }
}

function cfg_to_array( $cfg, $var )
{
	$lst = explode( " ",$cfg[$var] );
	while (list($k,$v) = each($lst)) {
		$cfg_lst[$v] = $cfg[$var."_$v"];
	}

	return $cfg_lst;
}

/** set user selected image size
*
*/
function set_user_image_size( $this, $_is )
{
	global $session;

	if ( $_is != '' ) {
		if ( $_is >=0 && $_is <= 4 ) {
			$session['uimg_size'] = $_is;
		}
	}
	else if ( $session['uimg_size'] == '' ) {
		$session['uimg_size'] = 3;
	}
	$_is = $session['uimg_size'];
	$_uimg_size = array( '_','_','_','_','_' );

	$_uimg_size[ $_is ] = '';

	// set which scale size is selected
	$ret =  array(
		'iscale0' => $_uimg_size[0],
		'iscale1' => $_uimg_size[1],
		'iscale2' => $_uimg_size[2],
		'iscale3' => $_uimg_size[3],
		'iscale4' => $_uimg_size[4],
		);

	// set scale size descriptions (resolution)
	for ($i=0; $i<5; $i++) {
		$ret[ 'iscale_txt'.$i ] = $this->config['size_'.$i];
		if ( $_is != $i ) {
			$ret[ 'scale_url'.$i ] = '?'.enc64($session['refurl'].'&is='.$i);
		}
		else {
			$ret[ 'scale_url'.$i ] = '#';
		}
	}

	return $ret;
}
