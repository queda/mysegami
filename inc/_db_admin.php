<?php	# $Id: _db_admin.php,v 1.1 2004-04-10 23:08:39 paulmcav Exp $
/*
	HOWTO:

	Form values:
		session['post_id'] == $_REQUEST['post_id']

	Callbacks:
		cb_cmd_handle( $db,$req )
		cb_dl_info()
		cb_blob_parse( $write,$tbl='',$row='' )
		cb_update_formreq( $req )
*/
function this_menu_handle( $this, $db, $dl_fmt='' )
{
	global $session;


	if ( $session['userid']!=0 ){
		
		// download tab-delim  / text file
		if ($_REQUEST['dl']!='') {
			db_download( $this, $db, $_REQUEST['dl'] );
			die;
		}
		
		// handle UPDATE / ANY kind of form action, if session POST_ID is set
		if ($session['post_id'] == $_REQUEST['post_id'])
		{
			if ( $_REQUEST['cmd'] == 'Update' ){
				db_info_update( $this, $db, $_REQUEST['f_id'] );
			}
			if ( $_REQUEST['cmd'] != '' ){
#			if (method_exists( $this, 'cb_cmd_handle' )
				$this->cb_cmd_handle( $db,$_REQUEST );
			}
		}
	   	else if ( $_REQUEST['post_id']=='' && $_REQUEST['cmd']!='' ) { echo "missing post-id<br>"; }
		
		$this->t->set_var( array(
#					'tbl_ed'    => '',
					'ed_cl'     => 'edit_bg',
					));
	}
	else {
		// clear the edit header / footer stuff
		$this->t->set_block('body','pre_admin_edit','x');
		$this->t->set_block('body','post_admin_edit','x');
	}
}


function db_info_update( $this, $db, $r_id=0 )
{
	global $session;

	// get list of file objects
	$flist = $this->cb_blob_parse(0);

	if (is_array($flist)) {

		// check for tab-delim upload file
		$bp = 0;
		while ( $file=each($flist) ) {
			if ($file[1] != 'bin') {
				$bp += db_upload( $this, $db, $file, $r_id );
			}
		}
		if ($bp) return 0;
		
		// check for normal data upload file
		reset($flist);
		$_img_list = array();
		while ( $file=each($flist) ) {
			if ($file[1] == 'bin' && $_FILES[$file[0]]['name'] != "" ) {
				// handle binary data upload
				array_push( $_img_list, substr($file[0],2) );

#echo "img: $file[0]<br>";
				$_REQUEST[$file[0]]['name'] =
				   	move_upload_file( $_FILES[$file[0]] );
			}
		}
	}

	$db_tbl = $this->info['table'];
	$form_data = $this->cb_update_formreq($_REQUEST);
	
#echo "<Pre>"; print_r( $_REQUEST ); echo "</pre>";

	// no db info, lets add a new record
	if ( $r_id==0 ) {
		$sql = "INSERT INTO $db_tbl SET cd=NOW()";
		$swr = "";
	}
	// else we want to update existing info
	else {
		// unlink old - existing file(s)
		if (count($_img_list)) {
			
			$sql = "SELECT * FROM $db_tbl WHERE id=$r_id";
			$db->query( $sql );
			if ($db->next_record()) {
				while ( list($k,$v) = each($_img_list) ) {
					if ($db->Record[$v] != '')
						unlink( "data/".$db->Record[$v] );
				}
			}
		}

		// update database with current info
		$sql = "UPDATE $db_tbl SET id=id";
		$swr = " WHERE id=$r_id";
	}

	// create query string from form values
	$fs = form2query( $form_data );
# echo "sql: $sql$fs$swr<br>";
	if ( $fs!='' ){ $db->query( $sql.$fs.$swr ); }
}

function crlf2nl( $val )
{
	return str_replace( array('\t',"\r\n","\r"), array("\t","\n","\n"),
				$val );
}

function db_blob_parse_txt( $this, $db, $sql, $blob )
{
	$form_data = array();
	$headers = $this->cb_blob_parse( 1 );

#	echo "<pre>"; print_r( $headers );

	$cnt = 0;
	$l = 0;
	
	while ($line = array_shift($blob)) {
		
		$line = trim($line);
#	echo "line: ($l) $line\n";

		// found a field name
		if (substr($line,0,1)=='=' && substr($line,-1)=='=') {
			$nkey = strtolower(substr($line,1,-1));
			if ($headers[$nkey]==''){ $nkey = ''; 
				// increment the record counter
				if ($line=='==') {
					$val = crlf2nl( $val );
					$form_data[$cnt][ $headers[$key]."_$key" ] =
						implode("\n",$val);
					$cnt++;
					$l = 0;
				}
			}
			else {
				// save the old key / val into db.
				if ($key!='') {
					unset( $val[$l-1] );
					$val = crlf2nl( $val );
					$form_data[$cnt][ $headers[$key]."_$key" ] =
						implode("\n",$val);
#	echo "fd$cnt-$l: $key = $val[0]\n";
				}
				
				$l = 0;
				$key = $nkey;
				unset($val);
			}
		}
		else {
#	echo " val: ($l) $line\n";
			// save the line
			$val[$l++] = $line;
		}
	}
	// save last field in DB
	if ($l!=0) {
		unset( $val[$l] );
		$val = crlf2nl( $val );
		$form_data[$cnt][ $headers[$key]."_$key" ] = implode("\n",$val);
	}
#	else { echo "fd$cnt-$l: $key = $val[0]\n"; }
	
	for ($i=0; $i<$cnt; $i++) {
		// add id record count if we are not editing one record
		if ($r_id==0) {
			$form_data[$i][ $headers['id'].'_id' ] = $i+1;
		}
#	print_r( $form_data[$i] );
		$fs = form2query( $form_data[$i] );
#	echo "sql: ".$sql.$fs.$swr.";<br>\n";
		if ( $fs!='' ){ $db->query( $sql.$fs.$swr ); }
	}
	return $cnt;
}


function db_blob_parse_xls( $this, $db, $sql, $blob )
{
	$cnt = 0;
	while ($line = array_shift($blob)) {
		$row = explode("\t", rtrim($line));

		// 1st row, drop column headers
		if ($cnt++==0)
			continue;

		for ($i=0;$i<count($row);$i++) {

			// deal with xtra quotes crap
			if( substr($row[$i],0,1)=='"' &&
				substr($row[$i],strlen($row[$i])-1)=='"'
			  ){
				$row[$i] = stripcslashes(substr($row[$i],1,-1));
				$row[$i] = str_replace( '""', '"', $row[$i] );
			}
		}
		$row['id'] = $cnt-1;
		$form_data = $this->cb_blob_parse( 1,$r_name, $row );
#	echo "row: "; print_r($row); echo "\n";

		$fs = form2query( $form_data );
#	echo "sql: ".$sqlp.$fs.";<br>\n";
		if ( $fs!='' ){ $db->query( $sql.$fs ); }
	}
	return $cnt;
}


function db_upload( $this, $db, $f_info, $r_id=0 )
{
	list($r_name,$dl_fmt) = $f_info;
	
	if ($r_name=='') return 0;	

	$s_tbl = $this->info['table'];
	$r_data = $_FILES[$r_name];
	
#	header( "Content-type: text/plain");
#	echo "<pre>"; print_r( $r_data );

	// was some kind of file uploaded w/ data?
	if ($r_data['tmp_name']=='' || $r_data['tmp_name']=='none') {
		return 0;
	}
	
	// uploaded file has no data
	$blob = file($r_data['tmp_name']);
	if (!count($blob))
		return 0;

	if ($r_id>0) {
		$sql = "UPDATE $s_tbl SET id=id";
		$swr = " WHERE id=$r_id";
	}
	else {
		$sql = "INSERT INTO $s_tbl SET cd=NOW()";
		$swr = '';
		$db->query( "DELETE FROM $s_tbl" );
	}

	// f'd up magic quotes thing
	$gpc = ini_get('magic_quotes_gpc');
	ini_set('magic_quotes_gpc',0);

	$db->Halt_On_Error = 'report';

#	echo "dl_fmt: $dl_fmt\n";
	
	if ($dl_fmt=='txt') {
		$cnt = db_blob_parse_txt( $this, $db, $sql, $blob );
	}
	else if ($dl_fmt=='xls') {
		$cnt = db_blob_parse_xls( $this, $db, $sql, $blob );
	}
	else {
		header( "Content-type: text/plain");
		echo "Error, unknown file type to parse.\n";
		die;
	}

	$db->Halt_On_Error = 'yes';
	ini_set('magic_quotes_gpc',$gpc);

	return $cnt;
}


function db_download( $this, $db, $r_id=0 )
{
	$inf = $this->cb_dl_info();

	$file   = $inf['file'];
	$dl_fmt = $inf['type'];
	$header = $inf['header'];
	$order  = $inf['order'];
	$tbl    = $inf['table'];

	if ($r_id>0) {
		$swr = " WHERE id=$r_id";
		$rnm = "_$r_id";
	}
	$sql = "SELECT $header FROM $tbl t $swr ORDER BY $order";

	$db->query( $sql );

	Header( "Content-type: text/plain" );
#	Header( "Content-type: application/vnd.ms-excel" );
	Header( "Content-disposition: attachement; filename=\"$file$rnm.$dl_fmt\"" );

	$hlist = explode(",", strtoupper($header));
	$cnt = count($hlist);
	
	if ($dl_fmt=='xls'){ echo join("\t",$hlist)."\n"; }
	while ($db->next_record()) {
		$row = $db->Record;
		for ($c=0; $c<$cnt ; $c++){
			if ($dl_fmt=='xls') {
				echo $row[$c].($c!=($cnt-1) ? "\t":'');
			}
			else {
				echo "=".$hlist[$c]."=\n";
				echo $row[$c]."\n\n";
			}
	   	}
		if ($dl_fmt!='xls') { echo "=="; }
		echo "\n";
	}
}

function db_delete_images( $db, $tbl, $base, $max, $sql )
{
	$db->query( $sql );

	$in_list = "";
	while ( $db->next_record() ){
		$row = $db->Record;

		$d = 0;
		// check if file is associated
		for( $i=0; $i<$max; $i++ ) {
			$fname = $row[$base.$i];
			if ($fname != "") {
				$d = 1;
				unlink( "data/$fname" );
			}
		}
		if ( $d == 1 )
			$in_list .= ($in_list!="" ? "," :"").$row['id'];
	}
	if ( $in_list != "" ){ 
		$sql = "DELETE FROM $tbl WHERE id IN($in_list)";
		$db->query( $sql );
#echo "sql: $sql<br>";
	}
}


