<?php # $Id: AddNew.cl.php,v 1.6 2004-07-27 23:30:40 paulmcav Exp $

IncludeObject('.','db_mysql');

/** AddNew page
*
*/
class AddNew extends Smarty
{
	var $name = "AddNew";
	var $title = "Add New: ";
	var $db;
	var $config;

	function AddNew()
	{
		global $session; 
	
		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		if ( $session['userid'] == '' ){ echo "need to login.<br>"; die; }

		$add_new = $session['add_new'];

		if ( !count($add_new['img']) ){ echo "No images.<br>"; die; }

		$_path = $add_new['dir'];
		
//		echo "<pre>"; print_r( $add_new['img'] ); echo "</pre>";
//		echo "<pre>"; print_r( $_REQUEST ); echo "</pre>";

		$_CMD       = $_REQUEST['cmd'];
		$f_ADD      = $_REQUEST['f_add'];
		$f_KEYWORDS = $_REQUEST['f_keywords'];
		$f_ORDER    = $_REQUEST['f_ordr'];
		
		if ( $_CMD == 'Add Selected' && count($f_ADD) ) {
			$_add_list = 
				$this->do_add_images($this->db, $add_new, $f_ADD, $f_KEYWORDS
				, $f_ORDER );
			unset( $session['add_new'] );
			unset( $session['view'] );
		}
		
//		echo "base,file: $base, $file<br>";

		$this->assign( array(
			'page_title' => $this->title,
			'path' => $_path,
			'add_list' => $_add_list,
			)
	   	);

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------

	function do_add_images( $db, $add_new, $add_list, $keywords, $order )
	{
		global $session;

		$_path = $add_new['dir'];

		while ( list($k,$v)=each($add_list) ){
			$_file = $add_new['img'][$k]['fname'];
			$_t    = $add_new['img'][$k]['type'];

//			echo "add img: $_file<br>";

			// adding a video clip, needs to be processed.
			if ( $_t == 'v' ) {
				$ex_file = $this->do_queue_vid_clip( $_path, $_file );
			}
			else {
				$ex_file = "$_path/$_file";
			}
			$exif = do_read_exif_data( $this->config['img_dir']."/$ex_file" );
			$comp = $exif['COMPUTED'];

			$_fpath = strstr($_path,'/');
			
			$i_res = $comp['Width'].' x '.$comp['Height'];
			$i_cd  = $exif['DateTimeOriginal'];
			
//			echo "<pre>"; print_r( $exif ); echo "</pre>";
			
			$db->Halt_On_Error = 'No';
			/* look in db to find existing image / directory name */
			$sql = "SELECT * from image WHERE "
				." name='".addslashes($_file)."'"
				." AND dir='".addslashes($_fpath)."'"
				;
			$db->query( $sql );
			if( $db->next_record() ){
				$row = $db->Record;
				$iid = $row['id'];
				
				/* image in database already (image_name,dir) */

				$sql = "UPDATE image SET ts=ts"
					.",dir='".addslashes($_fpath)."'"
					." WHERE id=$iid"
					;
#				$db->query( $sql );
			}
			else {
				/* insert new image into DB */
				$sql = "INSERT INTO image SET"
					." user_id=".$session['userid']
					.",name='".addslashes($_file)."'"
					.",dir='".addslashes($_fpath)."'"
					.",media='$_t'"
					.",status='a'"
					.",cd='$i_cd'"
					.",kwds='".strtolower(addslashes($keywords))."'"
					.",ordr='$order'"
//					.",priv=5"
					;
//				echo "sql $sql<br>";
				$db->query( $sql );
				$iid = $db->get_last_insert_id('image','id');
			}
			
			$session['srv_id'] = 2;
			
			$i_size = filesize($this->config['img_dir']."/$_path/$_file");
			
			/* add the image / site link */
			$sql = "REPLACE INTO site_image SET"
				." image_id=$iid"
				.",server_id=".$session['srv_id']
//				.",server_id=5"
				.",res='$i_res'"
				.",size=$i_size"
				;
//			if ( $iid ){ echo "sql: $sql<br>"; }
			if ( $iid ){ $db->query( $sql ); }
			
			$db->Halt_On_Error = 'Yes';
			
			$_add_list[] = array(
				'name' => $_file,
				'res'  => $i_res,
				'size' => $i_size,
				'type' => $_t,
				'cd'   => $i_cd,
			);
		}
		return $_add_list;
	}

	function do_queue_vid_clip( $path, $file )
	{
		$ext = substr( $file,-3 );
		$fname = substr( $file,0,-4 );
		
		$base = $this->config['img_dir']."/$path";
		
		// test for thumbnail image, and/or generate one
		if ( $ext == 'avi' ) {
			$tfile = "$fname.thm";
		}
		else if ( $ext == 'AVI' ) {
			$tfile = "$fname.THM";
		}
		else {
		}

		$qfile = $this->config['img_dir']."/mpegq/$fname.job";
		$qdata = "$path/$file\n";
		
		if ( $tfile != '' && !file_exists( "$base/$tfile" ) ) {
			$qdata .= "thumb\n";
		}

		$fd = fopen( $qfile,"w" );
		fwrite( $fd, $qdata );
		fclose( $fd );
		
		return "$path/$tfile" ;
	}
}

include_once( '_exif.php' );
