<?php # $Id: Help.cl.php,v 1.1 2004-05-27 22:10:35 paulmcav Exp $

//IncludeObject('.','db_mysql');
//IncludeObject('.','email');

/** Help page
*
*/
class Help extends Smarty
{
	var $name = "Help";
	var $title = "MySegami Help";
	var $db;
	var $config;

	function Help()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

//		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		$this->assign( array(
			'page_title' => $this->title,
			'refurl'     => enc64( "page=Help" ),
			'referer'    => $_SERVER['HTTP_REFERER'],
			'head_title' => 'User Help',
			)
	   	);

		// final process... output page
		$out = $this->fetch( $this->name.".html" );
		$this->assign( "body", $out );
		$this->display( "common.html" );
	}

	// ----------------------
}

