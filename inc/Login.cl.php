<?php # $Id: Login.cl.php,v 1.5 2004-05-27 22:10:35 paulmcav Exp $

IncludeObject('.','db_mysql');
IncludeObject('.','email');

/** Login page
*
*/
class Login extends Smarty
{
	var $name = "Login";
	var $title = "MySegami Login";
	var $db;
	var $config;

	function Login()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

		$this->assign( array(
			'page_title' => $this->title,
			'refurl'     => enc64( "page=Login" ),
			'head_title' => 'Login Page',
			)
	   	);

		$_CMD = $_REQUEST['cmd'];
		$_UID = $_REQUEST['uid'];
		$_PWD = $_REQUEST['pass']; 
		
		// send a user password
		if ( $_CMD == 'Send Password' || $_CMD == '2' ) {
			if ( $this->request_pass( $_REQUEST['uid']) ) {
				$_mode = 'snd_pass';
			}
			// invalid user
			else {
				$_mode = 'snd_nopass';
			}
		}

		// log user out
		if ( $_CMD == 'Logout' ) {
			$msg = "You have been logged out!";

			unset( $session['loginok'] );
			unset( $session['userid'] );
			unset( $session['userem'] );

			setcookie( 'userid', '',time()-3600,'/' );

			$_mode = 'logout';
		}

		// log user in
		if ( $_CMD == 'Login' ) {

			$err=ValidateUser($this->db, $this->config, $_UID, $_PWD);
			
			if ( $err >= 0 ) {
				if ( $session['refurl'] != '' )
					$location = $session['refurl'];
				else
					$location = enc64("page=Index");

//				echo "location: $location<br>";
				Header( "Location: ?$location" ); die();
			}
			// login failed
			else if ( $err == -2 ) {
				$_mode = 'li_fail_upw';
			}
			else {
				$_mode = 'li_fail_uid';
			}
		}

		$this->assign( 'li_mode', $_mode );
		
		// final process... output page
		$out = $this->fetch( $this->name.".html" );
		$this->assign( "body", $out );
		$this->display( "common.html" );
	}

	// ----------------------

	function request_pass( $uid )
	{
		$ret = FindUser( $this->db, $uid,'',$row );

		if ( !is_array( $row ) )
		{
			return 0;
		}
		
		// try and send email w/ the password to the found user.
		$email = new email('msg_subscribe');

		$email->assign( array(
			'name'  => $row['name'],
			'email' => $row['email'],
			'pass'  => $row['passwd'],
			'uid'   => $row['id'],
			'h_url' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
			) );

		$email->send(
			$this->title.' Password Request',
//			'paulmcav',
			$row['email'],
			'paulmcav@queda.net'
			);
		return 1; 
	}
}

