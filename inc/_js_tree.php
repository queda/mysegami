<?php # $Id: _js_tree.php,v 1.1 2004-05-03 20:49:08 paulmcav Exp $

function jst_build_output( $list, $level, $maxlvl, &$output
	, $is_last = array(), $fClick='' )
{
	if ( $maxlvl && $level >= $maxlvl ){ return; }
	
	$node = $list['/'];
	$max  = count($node);
	$id   = $list[0]['id'];
	
	if ( $level )
		$output[] = array( 'div' => '<div id="folder'.$id.'">' );
		
	$cnt = 0;
	while ( $max && list($k,$v) = each( $node ) ){
	
//		echo "($level) k/v: $k/$v<br>";
//		echo "d: $v[d_cnt]<br>";
//		echo "<pre>"; print_r( $v ); echo "</pre>";

		$is_last[$level] = (++$cnt == $max);
		$d_cnt           = $v['d_cnt'];
		
		$v[0]['images'] =
			jst_gen_tree_images( $level,$v[0]['id'],$d_cnt,$is_last,$fClick );

		// has sub-dirs
		if ( $d_cnt ){

		   	$v[0]['img_cnt'] = $v[0]['img_cnt']."/$d_cnt";
			$output[] = $v[0];

			jst_build_output( $v, $level+1, $maxlvl, $output, $is_last,$fClick );
		}
		else {
			$output[] = $v[0];
		}
	}
	if ( $level )
		$output[] = array( 'div' => '</div>' );
}

function jst_build_tree( $list, $id=0, $level=0 )
{
	$max = count( $list[$id] )-1;
	
	if ( $max<0 ){ return; }
	
	$result['level'] = $level;
	$result['d_cnt'] = $max+1;
	
	while ( list($i,$node) = each( $list[$id] ) ) {

		$p_id = $node['id'];
		$n_id = $node['name'];
		
		if ( is_array( $list[ $p_id ] ) ) {
			
			$result['/'][$n_id] 
				= jst_build_tree( $list, $p_id, $level+1 );
		}

		$result['/'][$n_id][0]         = $node;
		$result['/'][$n_id][0]['last'] = ($i == $max);
	}
	return $result;
}

function jst_onClick( $id ) {
	return "toggleFolder('folder$id',this)";
}

function jst_gen_tree_images( $level, $id, $is_folder, $is_last, $click='' )
{
	$out = '';

	if ( $click=='' ){ $click = 'jst_onClick'; }
	
	for ( $i=0; $i<$level; $i++ ){
		if ( $is_last[$i] ) {
			$out .= '<img src="images/ftv2blank.png" alt="|" width=16'
			.' height=22 />'
			;
		}
		else {
			$out .= '<img src="images/ftv2vertline.png" alt="|" width=16'
			.' height=22 />'
			;
		}
	}
	if ( $is_folder ) {
		if ( $is_last[$level] ) {
			$out .= '<img src="images/ftv2plastnode.png" alt="o" width="16"'
			 .' height="22" id="'.$id.'" onclick="'.$click($id).'"'
			 .' />'
			 ;
		 } else
		 {
			$out .= '<img src="images/ftv2pnode.png" alt="o" width="16"'
			 .' height="22" id="'.$id.'" onclick="'.$click($id).'"'
			 ." />"
			 ;
		 }
		$out .=
		  '<img src="images/ftv2folderclosed.png" alt="+" width="24"'
		 .' height="22" onclick="'.$click($id).'"'
		 ." />"
		 ;
	}
	else {
		if ( $is_last[$level] ) {
			$out .= 
			 '<img src="images/ftv2lastnode.png" alt="o" width="16"'
			 .' height="22"'
			 ." />"
			 ;
		 }
		 else {
			$out .= 
			 '<img src="images/ftv2node.png" alt="o" width="16"'
			 .' height="22"'
			 ." />"
			 ;
		 }
		$out .=
		  '<img src="images/ftv2doc.png" alt="*" width="24"'
		 .' height="22"'
		 ." />"
		 ;
	}
	return $out;
}



function jst_get_collections( $db, $user_id )
{
	$b_status = "1";
	$srv_id = 1;

	$sql = "SELECT b.*,"
		  ."SUM(IF(bi.image_id is not null,1,0)) AS img_cnt"
		  .",DATE_FORMAT(b.cd,'%d%b%y') AS _cd,si.server_id"
		  ." FROM book b"
		  ." LEFT JOIN book_image bi ON b.id=bi.book_id"
		  ." LEFT JOIN site_image si ON bi.image_id=si.image_id"
		  ." WHERE $b_status AND b.user_id=$user_id"
//			  . " AND b.id in (58,48) or b.p_id in (48,58) "
#				  ." AND si.server_id=".$session['srv_id']
		  ." GROUP BY b.id"
//			  ." HAVING (si.server_id IS NULL OR si.server_id=$srv_id)"
		  ." ORDER BY b.cd";

//echo "sql: $sql<br>";

	$db->query( $sql );
	while ( $db->next_record() ) {
		$row = $db->Record;

		$node = array(
			'p_id' => $row['p_id'],
			'id'   => $row['id'],
			'name' => $row['name'],
			'_cd'  => $row['_cd'],
			'img_cnt' => $row['img_cnt'],
			'image_url' => enc64("s=5&dbid=".$row['image_id']),
			'clctn_url' => enc64( "page=View&bkid=".$row['id'] ),
			'notes' => $row['notes'],
		);

		$list[$row['p_id']][] = $node;
	}

	return $list;
}
