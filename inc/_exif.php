<?php # $Id: _exif.php,v 1.2 2004-06-10 04:27:56 paulmcav Exp $
	
function do_read_exif_data($path) {
	if ( !is_file($path) ) return;

#		$tmpfile = "/var/tmp/exif.dat";
#		$in = fopen($path, "r");  
#		$out = fopen($tmpfile,"w");
#		fwrite( $out, fread( $in, 15000 ) );
#		fclose($out); fclose($in);
#		$exif = read_exif_data($tmpfile);
#		unlink($tmpfile);
	$exif = @read_exif_data($path);
	
	if (!isset($exif['DateTimeOriginal']) 
		|| intval($exif['DateTimeOriginal']) == 0 ){
		$exif['DateTimeOriginal'] = date ("Y-m-d H:i:s", filemtime($path));
	}
	/*
	*/
   
	$delkeys = array(
		'FileName',
		'FileDateTime',
		'FileSize',
		'FileType',
		'SectionsFound',
		'ExifVersion',
		'PictureInfo',
		'CameraId',
		'MakerNote',
	);

	// remove unwanted keys
	foreach( $delkeys as $n => $k ){
		unset( $exif[$k] );
		unset( $exif[ strtoupper($k) ] );
	}

//	echo "<pre>"; print_r( $exif ); echo "</pre>";
	
	// remove any keys w/ empty data
	foreach( $exif as $k => $v ){
		if ( ord($v)<32 )
			unset($exif[$k] );
	}
	return $exif;
} 

	
