<?php # $Id: View.cl.php,v 1.21 2004-06-13 23:11:04 paulmcav Exp $

IncludeObject('.','db_mysql');

/** View page
*
*/
class View extends Smarty
{
	var $name = "View";
	var $title = "Segami Weiv";
	var $db;
	var $config;

	var $_Image_Rez;

	function View()
	{
		global $session; 
	
		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;
		
		// interested in what type of browser is being used
        if(eregi("Windows",$_SERVER['HTTP_USER_AGENT'])) $_OS = "WIN";
        if(eregi("Mac",$_SERVER['HTTP_USER_AGENT'])) $_OS = "MAC";

		$this->assign( array(
			'page_title' => $this->title,
			'os_type' => $_OS,
			)
	   	);

/*
		if ( $_REQUEST['d'] == 1 ){ echo "running db_update<br>";
			$this->run_db_update(); die; }
*/
		
		$this->_Image_Rez = cfg_to_array( $this->config, 'size' );
//		echo "<pre>"; print_r( $this->_Image_Rez ); echo "</pre>";
		
		$_UID  = $_REQUEST['uid'];
		$_DIR  = $_REQUEST['dir'];
		$_BKID = $_REQUEST['bkid'];
		$_DBID = $_REQUEST['dbid'];
		$_P    = $_REQUEST['p'];
		$_CMD  = $_REQUEST['cmd'];
		$_TGGL = $_REQUEST['toggle'];
		$_DEL  = $_REQUEST['del'];
		
		$session['page_size'] = 40;
		
		// nothing passed, check session for state
		if ( $_DIR == '' &&  $_BKID == '' ) {
			$m = $session['view']['md'];
			$i = $session['view']['id'];

			if ( $m == 'dir' ) {
				$_DIR = $i;
			}
			else if ( $m == 'bkid' ) {
				$_BKID = $i;
			}
		}
		
		// dbid only was passed
		if ( $_DBID != '' ) {
//			$info = $this->get_image_info( $this->db, $_DBID );
			$session['refurl'] .= "&dbid=$_DBID";
		}
		
		// get uid from 1st part of dir path, override REQUEST uid
		if ( $_DIR != '' ) {
			$path_parts = explode( '/', $_DIR,2 );
			$_UID = $path_parts[0];
			$session['refurl'] .= "&dir=".urlencode($_DIR);
		}

		// if a BKID passed, use for uid
		if ( $_BKID != '' ) {
			$sql = "SELECT user_id FROM book WHERE id=$_BKID";

			$this->db->query( $sql );
			if ( $this->db->next_record() ) {
				$_UID = $this->db->Record['user_id'];
			}
			$session['refurl'] .= "&bkid=$_BKID";
		}

		// discern a uid if passed, and set to 0 if not avail
		if ( $_UID != '' ) { $session['view']['uid'] = $_UID; }
		$_UID = $session['view']['uid'];
		if ( $_UID == '' ) { $_UID = 0; }

		$User_Info = $this->get_user_info( $this->db, $_UID );

		// -- set requested image size
		$this->assign( set_user_image_size( $this, $_REQUEST['is'] ) );
		
		// -- correct page request size
		if ( $_P>0 ) { $_P -= 1; }
		
		// deal with some form stuff (user has to be logged in)
		if ( $session['userid'] ) {
			if ( $_CMD == "Add Comment" ){
				$_F_dbid    = $_REQUEST['f_dbid'];
				$_F_comment = $_REQUEST['f_comment'];

				$this->do_update_comment( $session['userid'], $_F_dbid, $_F_comment );
			}
			if ( $_TGGL > 0 ) {
				$this->do_update_access( $this->db, $session['userid'], $_TGGL );
			}
			if ( $_DEL > 0 ) {
				$this->do_del_image( $this->db, $session['userid'], $_DEL );
			}
		}
		
		// looking at selected image
		if ( $_DBID != '' ) {
			$name = $this->do_image_view( $this->db, $_UID, $_DBID, $_DIR
				, $session['uimg_size'] );
			$_IMG = " : ".$name;
			$_view_mode = 'i';

			// -- generate image size display info
			$this->assign( 'v_size_select','1' );
			unset( $session['add_new'] );
//			echo "1unset<br>";
		}

		$_slide_url = '';

		// looking at a book vs directory
		if ( $_BKID != '' ) {
			$_DIR = $this->do_book_nav( $this->db, $_UID,$_BKID, $_P);

			#$_md = 'bkid';
			#$_id = $_BKID;
			#$ref = "&bkid=$_BKID";
			$_view_mode = 't';
			$_path_url = "bkid=$_BKID";
		}
		// looking at a directory
		else {
			$this->do_dir_nav($_UID,$_DIR);

			if ( $_DBID == '' ) {
				$this->do_dir_thumbs( $this->db, $_UID, $_DIR, $_P );
				$_view_mode = 't';
			}

			$paths = explode( '/',$_DIR,2 );
			
			#$_md = 'dir';
			#$_id = $_DIR;
			#$ref = "&dir=$_DIR";
			$_path_url = "dir=".urlencode($_DIR);
			
			if ( $session['userid'] != '' && $_DIR != '' ) {
				$_slide_url = enc64("page=ViewSlide&dir=".urlencode($_DIR));
			}
//			$_DIR = $paths[1];
		}

		if ( isset( $session['add_new'] )
			&& ($_DIR != $session['add_new']['dir'] )
		) {
			unset( $session['add_new'] );
//			echo "2-$_DIR-unset<br>";
		}
		
		#$session['view_md'] = $_md;
		#$session['view_id'] = $_id;
		#$session['refurl'] .= $ref;

		// edit popup url
		$_wJSOL = $_REQUEST['wJSOL'];
		$_js_onload = "";
		if ( $_wJSOL & 0x01 ) {
			$_js_onload .= "EditInfo=urlopen('EditInfo');";
		}
		if ( $_wJSOL & 0x02 ) {
			$_js_onload .= "ViewExif=urlopen('ViewExif');";
		}

		$this->assign( array(
			'_uid'        => $_UID,
			'view_mode'   => $_view_mode,
			'path_url'    => '?'.enc64( "page=View&$_path_url"),
			'path_select' => $paths[1],
			'path_image'  => $_IMG,
			'slide_url'   => $_slide_url,
			'head_title'  => $User_Info['name'], //." - ".$_DIR,
			'refurl'      => '?'.enc64($session['refurl']),
			'js_onload'   => $_js_onload,
			)
		);

		// final process... output page
		$out = $this->fetch( $this->name.".html" );
		$this->assign( "body", $out );
		$this->display( "common.html" );
	}

	// ----------------------

	/** View selected image details
	*
	*/
	function do_image_view( $db, $_UID, $_DBID, $_DIR, $size=3 )
	{
		global $session;

		// only owner can see non 'a'vailable records
		if ( $session['userid'] != $_UID ) {
			$status = "AND status='a'";
		}

		$paths = explode( '/', $_DIR, 2 );
		
		if ( $session['view']['p_id'] != $_DIR ) {
			$sql = "SELECT i.name,i.id iid,si.id siid"
				." FROM image i"
				." LEFT JOIN site_image si ON i.id=si.image_id"
				." WHERE i.user_id=$_UID"
				." AND si.server_id=2"
	//			." AND si.server_id=".$session['srv_id']
				." AND i.dir='/".addslashes($paths[1])."' $status"
				." AND i.media!='v'"
				." ORDER by media,cd,name";

			$db->query( $sql );
			while ( $db->next_record() ) {
				$id_list[] = $db->Record['iid'];
			}

			$session['view']['p_id'] = $_DIR;
			$session['view']['v_id'] = $id_list;
		}
		else {
			$id_list = $session['view']['v_id'];
		}

		// scan list of id's to find next / prev images
		$i_num = -1;
		$i_max = count($id_list);
		for ( $i_cnt = 0; $i_cnt < $i_max; $i_cnt++ ) {
			if ( $id_list[$i_cnt] == $_DBID ){ $i_num = $i_cnt; }
		}

		// image was not found in selection list.. assume video clip
		if ( $i_num < 0 ) { $i_num = $i_max; }
		$i_prv = $i_num-1; 
		$i_nxt = $i_num+1;
//	echo "p,c,n: $i_prv, $i_num, $i_nxt, $i_max<br>";
//	echo "<pre>"; print_r( $id_list ); echo "</pre>";
		
		$url_pagedir = "page=View&dir=".urlencode($_DIR);
		
		$img_nav['prv_url'] = "#";
		$img_nav['nxt_url'] = "#";
		
		$img_nav['cur'] = $i_num+1;
		$img_nav['max'] = $i_max;
		$img_nav['position'] = ($i_num+1).'/'.$i_max;
		
		$cur_page = floor(($i_num / $session['page_size']) + 1);
		$img_nav['page_url'] = enc64($url_pagedir."&p=$cur_page");

		if ( $i_prv>=0 ) { 
			$img_nav['prv_url'] =
			'?'.enc64($url_pagedir."&dbid="
				.$id_list[$i_prv]);
			$img_nav['prv_img'] = enc64("g=1&s=$size&dbid=".$id_list[$i_prv]);
	   	}
		if ( $i_nxt<$i_max) { 
			$img_nav['nxt_url'] =
			'?'.enc64($url_pagedir."&dbid="
				.$id_list[$i_nxt]);
			$img_nav['nxt_img'] = enc64("g=1&s=$size&dbid=".$id_list[$i_nxt]);
	   	}

		// --- select and display an image ---

		$img_data = get_image_data( $this, $_UID, $_DBID, $size );
		
		/*
		// select requested records
		$sql = "SELECT i.*,i.id iid,si.*,si.id siid,"
			." DATE_FORMAT(cd,'%d%b%y %H:%i') nicedate"
			." FROM image i"
			." LEFT JOIN site_image si ON i.id=si.image_id"
			." WHERE i.user_id=$_UID"
			." AND si.server_id=2"
//			." AND si.server_id=".$session['srv_id']
			." AND i.id=$_DBID $status";

		$scale = $this->_Image_Rez[ $size ];

		$db->query( $sql );
		if ( $db->next_record() ){
			$row = $db->Record;
			$data = $this->make_image_info( $this, $row, 1, $scale, $size );
//			echo "<pre>"; print_r( $data ); echo "</pre>";
		
			// get user notes
			$sql = "SELECT u.*,user_id,note,n.ts"
				." FROM image_note n LEFT JOIN"
				." user u ON n.user_id=u.id WHERE image_id=$_DBID";
#	echo "sql: $sql<br>";			
			$this->db->query( $sql );
			
			$db->query( $sql );
			while ( $db->next_record() ) {
				$row = $db->Record;
//			echo "<pre>"; print_r( $row ); echo "</pre>";

				// skip blank notes
				if ( $row['note'] == '' ) { continue; }
				$comment[] = array(
					'email' => $row['email'],
					'name'  => $row['name'],
					'note'  => $row['note'],
					'uid'   => $row['id'],
					'cd'    => $row['ts'],
				);

				// track users own comment for the html form
				if ( $session['userid'] == $row['user_id'] ) {
					$user_comment = $row['note'];
				}
			}
		}
		*/
		
//		$data = $img_data[0];
//		$comment = $img_data[1];
//		$user_comment = $img_data[2];
		$this->assign( array(
			'image'        => array_merge($img_data[0],$img_nav),
			'comment'      => $img_data[1],
			'user_comment' => $img_data[2],
	   	) );
		return $img_data[0]['name'];
	}
	
	function do_dir_nav( $_UID, $_DIR )
	{
		$paths = explode( '/', $_DIR,3 );

//		echo "parts: "; print_r( $paths ); 

		$exts = ".jpg.avi.mpg.mpeg.qt";
			
		/* draw top dir tree */
		$topdir = array();
		$dir_list = $this->build_dir_tree($_DIR,"$_UID/",0,2,'', $exts );
		jst_build_output( $dir_list, 0,1, $topdir,'','View_dir_click' );
		
		$this->assign( array(
			'topdir' => $topdir,
			'nav_mode' => 'dir',
	   	) );

		/* selected a dir, draw sub trees */
		if ( $_DIR != '' ){
			
			if ( $dir_list['/'][$paths[1]][0]['last'] != '') {
				$subdir_spacer_img = 
					'<img src="images/ftv2blank.png" width="16" height="22" />';
			}
			else {
				$subdir_spacer_img = 
					'<img src="images/ftv2vertline.png" alt="|" width=16 height=22 />';
			}
			
//			echo "dir: $_DIR = $paths[0]/$paths[1]<br>";
			
			$subdir = array();
			$dir_list =
				$this->build_dir_tree($_DIR,"$paths[0]/$paths[1]/",0,0,'', $exts);
//		echo "<pre>"; print_r( $dir_list ); echo "</pre>";
			
			jst_build_output( $dir_list, 0,0, $subdir,'' );
			
			$this->assign( array(
				'js_openfolder'     => "openFolders('$_DIR')",
			   	'subdir'            => $subdir,
		   		'subdir_spacer_img' => $subdir_spacer_img,
				) );
	   	}
	}

	function find_branch( $tree, $id, &$top_id )
	{
		$node = $tree['/'];
		$max = count($node);

		while( $max && list($k,$branch) = each($node) ) {
			
//			echo "k(".$branch[0][id].",$k)<br>";
			
			if ( $branch[0]['id'] == $id ) {
				$list = array();
				$list['level'] = 0;
				$list['d_cnt'] = 1;
				$list['/'][$k] = $branch;
//				$list['/'][$k][0]['last'] = 'last';
				return $list;
			}
					
			if ( ($list = $this->find_branch( $branch, $id, $top_id )) != 0 ) {
//				$list['level'] = 0;
//				$list['d_cnt'] = 1;
//				$list['/'][$k] = $branch;
//				$list['/'][$k][0]['last'] = 'last';
				/*
				if  ( $branch['level'] > 0 ) {

					$top_id = $k;
					return $list;
				}
				else 
				*/
				return $list;
			}
		}
		return 0;
	}
	
	function do_book_nav( $db, $_UID, $_BKID)
	{
		$list = jst_get_collections( $db, $_UID );
//		echo "<pre>"; print_r( $list ); echo "</pre>";

		$book_tree = jst_build_tree( $list );
//		echo "<pre>"; print_r( $book_tree ); echo "</pre><hr>";

		// return tree with branch in it
		$book_tree = $this->find_branch( $book_tree, $_BKID, $top_id );
//		echo "<pre>"; print_r( $book_tree ); echo "</pre><hr>";

		$subdir = array();
		jst_build_output( $book_tree, 0,0, $subdir );
//		echo "<pre>"; print_r( $subdir ); echo "</pre>";

//		$subdir = array();
//		jst_build_output( $book_tree['/'][$top_id], 0,0, $subdir,'' );
//		echo "<pre>"; print_r( $subdir ); echo "</pre>";

		$this->assign( array(
			'subdir' => $subdir,
//			'subdir_spacer_img' => 
//				'<img src="images/ftv2blank.png" width="16" height="22" />',
			'nav-mode' => 'book',
		) );
		
		return $_DIR;
	}
	
	/** Get user informatio from uid
	*
	*/
	function get_image_info( $db, $_DBID )
	{
		$sql = "SELECT * FROM image WHERE id=$_DBID";
		$db->query( $sql );

		if ( $db->next_record() ) {
			$row = $db->Record;
		}

		return $row;
	}

	/** Get user informatio from uid
	*
	*/
	function get_user_info( $db, $user_id )
	{
		$sql = "SELECT * FROM user WHERE id=$user_id";
		$db->query( $sql );

		if ( $db->next_record() ) {
			$row = $db->Record;
		}

		return $row;
	}

	/**
	*
	*/
	function get_dir_branch( $dir_list, $branch )
	{
//		echo "branch: $branch<br>";

		$parts = explode( '/', $branch, 3 );

//		while ( list($k,$v) = each( $parts ) ){ echo "k/v: $k=$v<br>"; }

		if ( count($parts) < 3 ) {
			return $dir_list['/'][$parts[1]];
		}
		else {
			return $this->get_dir_branch(
				$dir_list['/'][$parts[1]], '/'.$parts[2] );
		}
	}

	/** Generate a file query with a select limit / pagesize
	*
	*/
	function get_db_fileqry( $db, $_UID, $bdid, &$max_row, $_P=0, $len=0 )
	{
		global $session;
		
		// get count of records in query
		$sql = "SELECT count(*) from image"
				." WHERE user_id=$_UID AND dir='/".addslashes($bdid)."'"
			    ." AND STATUS='a'";

		$max_row = 0;

		$db->query( $sql );
		if ( $db->next_record() ) {
			$row = $db->Record;
			$max_row = $row[0];
		}

		// select requested records
		$sql = "SELECT i.*,i.id iid,si.*,si.id siid,"
			." DATE_FORMAT(cd,'%d%b%y %H:%i') nicedate"
			." FROM image i"
			." LEFT JOIN site_image si ON i.id=si.image_id"
			." WHERE i.user_id=$_UID"
//			." AND si.server_id=".$session['srv_id']
			." AND dir='/" .addslashes($bdid)."'";

		// only owner can see non 'a'vailable records
		if ( $session['userid'] != $_UID ) {
			$sql .= " AND status='a'";
		}
		$sql .= " ORDER BY media,cd,name";

//		echo "sql: $sql<br>";
		
		if ( $len ) {
			$_P = $_P * $len;
			$sql .= " LIMIT $_P,$len";
		}
		return $sql;
	}

	/** Generate pager selection info
	*
	*/
	function make_page_sel( $max, $_P, $size )
	{
		global $session;
//		echo "max: $max, _P: $_P<br>";

		for ( $i=0; $i<$max; $i+=$size ){
			$num = ($i/$size);
			$pager[ $num ]['num'] = $num+1;
			if ( $num == $_P ) {
				$pager[ $num ]['sel'] = 1;
			}
			else {
				$end = ($num+1)*$size;
				if ( $end > $max ){ $end = $max; }
				
				$pager[ $num ]['sel'] = 0;
				$pager[ $num ]['range'] = (($num*$size)+1)."-$end";
				$pager[ $num ]['url'] = enc64( $session['refurl']."&p=".($num+1) );
//				echo "i: ".(($i/$size)+1)."<br>";
			}
			if ( $_P < $num ){ 
				$pager['nxt_url'] = enc64( $session['refurl']."&p=".($_P+2) );
			}
		}

		$this->assign( array( 
			'pager' => $pager,
	   		'pages' => count($pager),
			) );

//		echo "<pre>"; print_r( $pager ); echo "</pre>";
	}

	/** Get a list of files from the database, and a path
	*
	*/
	function get_db_files( $db, $mode, $_UID, $bdid, $size=3, $_P)
	{
		global $session;

		$sql = $this->get_db_fileqry( $db, $_UID, $bdid, $max_row, $_P
	   		, $session['page_size'] );

		$this->make_page_sel( $max_row, $_P, $session['page_size'] );
		
		$scale = $this->_Image_Rez[ $size ];
			
		$db->query( $sql );
		while ( $db->next_record() ) {
			$row = $db->Record;

//			$data = $this->make_image_info( $this, $row, 0, $scale, $size );
			$data = make_image_info( $this, $row, 0, $scale, $size );

//			echo "<pre>"; print_r( $data ); echo "</pre>";

			$file_list[$row['media']][] = $data;
		}
		return $file_list;
	}
		
	/** Get a list of files from a directory path, by extension
	*
	*/
	function get_dir_files($path, $exts)
	{
		$dirpath = $this->config['img_dir']."/$path";
		
		if ( ($handle=@opendir($dirpath)) == 0 ){ return; }
		$cnt = 0;
		while ($file = readdir($handle)) {
			if ( $file != '.' && $file != '..' && !is_dir($dirpath.$file)) {
				$ext = substr( $file, strrpos( $file,'.' ) );
				// scan file extension for a valid type
				if ( stristr( $exts, $ext ) ) {
					$byname[$file]['id']  = $cnt;
					$byname[$file]['ext'] = $ext;
					$files[$cnt++] = $file;
				}
			}
		}
		closedir( $handle );
		
		if ( is_array($files) ){ asort( $files ); }
		if ( is_array($byname) ){ 
			ksort( $byname );
			$files['byname'] = $byname;
		}
//		echo "<pre>"; print_r( $files ); echo "</pre>";
		

		return $files;
	}
	
	/** build a directory tree from a given dir path
	*
	*/
	function build_dir_tree($sel, $path,$level,$maxdepth=0,$ppath='', $exts)
	{
		if ( $maxdepth && $level >= $maxdepth ){ return; }

//		echo "[$sel]($level) path: $path<br>";
		$dir_arr = array () ;
		
		$dc = 0; $fc = 0;

		$dirpath = $this->config['img_dir']."/$path";
		
		if ( ($handle=@opendir($dirpath)) == 0 ){ return; }
		while ($file = readdir($handle)) {
	    	if ($file != "." && $file != ".." && is_dir($dirpath.$file)) {
				$dir_arr[ $file ] = $dirpath.$file;
				$dir_arr[ $file ] = $path.$file;
				$dc++;
			}
			else if ( !is_dir($dirpath.$file) ) {
				$ext = substr( $file, strrpos( $file,'.' ) );
				// scan file extension for a valid type
				if ( stristr( $exts, $ext ) ) {
					$fc++;
				}
			}
		}
		closedir( $handle );
		
		asort( $dir_arr );

//		echo "$dirpath - $path, dc/fc: $dc/$fc<br>";

		$node['id']   = 0;
		$node['p_id'] = substr($path,0,-1);
		
//		$node['path']  = $path;
		$result['img_cnt'] = $fc;
		$result['level']   = $level;
		$result['d_cnt']   = $dc;
		
		$max = count($dir_arr)-1;
		$i = 0;
		while( list($file,$sub) = each( $dir_arr )) {
//				echo "file[$level]: $file = $sub<br>";
				
			    $sub_dir = $sub. "/" ;
//			    $dir_arr[] = $sub_dir ;       

			    $result['/'][$file] =
			   		$this->build_dir_tree($sel,
						$sub_dir,$level+1,$maxdepth,$file,$exts);

				$node['id']      = $path.$file;
				$node['name']    = $file;
				$node['img_cnt'] = $result['/'][$file]['img_cnt'];
				$node['last']    = ($i == $max) ? 'last' : '';
				$node['clctn_url'] = enc64("page=View&dir=".urlencode($path.$file));

				if ( "$path$file" == $sel ) {
					$node['select'] = "_sel";
				}
				else {
					$node['select'] = "";
				}
				
				unset( $result['/'][$file]['img_cnt'] );

				$result['/'][$file][0] = $node;

//				if ( ! $level ){ $result = $result['/']; }
				$i++;
		}
		return $result;
	}

	function find_new_files( $db, $_UID, $_DIR, $dir_files )
	{
		global $session;

		if ( !is_array( $dir_files ) ){ return; }
		
		$vid_types = ".avi.mpg.mpeg.qt";
		
		$paths = explode( '/', $_DIR, 2 );
		
		$sql = $this->get_db_fileqry( $db, $_UID, $paths[1], $max_row );

		$db->query( $sql );

		// remove any found dir files if already in database
		while( $db->next_record() ) {
			$row = $db->Record;

			$idx = $row['name'];
			if ( isset( $dir_files['byname'][$idx] ) ) {
				unset( $dir_files[ $dir_files['byname'][$idx]['id'] ] );
				unset( $dir_files['byname'][$idx] );
			}
		}

//		echo "<pre>"; print_r( $dir_files ); echo "</pre>";
		
		// reform the dir_files for web-page output
		while ( list($k,$v) = each($dir_files['byname']) ) {
			$ext = $dir_files['byname'][$k]['ext'];
			$exif_url = "page=ViewExif&f=";
			$exif_file = "$_DIR/";
			
			if ( stristr( $vid_types, $ext ) ) {
				$type = 'v';
				$e = substr($k,-3);
				$exif_file .= substr($k,0,-3);
				if ( $e == 'avi') { $exif_file .= "thm"; }
				if ( $e == 'AVI') { $exif_file .= "THM"; }
			}
			else {
				$type = 'i';
				$exif_file .= $k;
			}
			$exif_url .= urlencode( $exif_file );

//		"location.href='?".enc64($session['refurl']."&dbid=".$row['iid'])."';";
			$src_url = "open_win('media.php?"
				.enc64("s=3&f=".urlencode("$_DIR/$k"))."','photo','550','416');";
		
			$data[$type][] = array(
//			$data[] = array(
				'type'     => $type,
				'fname'    => $k,
				'name'     => substr( $k, 0,strrpos($k,'.') ),
				'img_thm'  => enc64("s=t&f=".urlencode("$_DIR/$k")),
				'img_src'  => $src_url,
				'exif_url' => enc64($exif_url),
				'img_tx'   => 0,
				'img_ty'   => 0,
			);

			$new_sdata[$type][] = array(
				'type'  => $type,
				'fname' => $k,
			);
		}

		// add new items to session data
		if ( count($new_sdata) ) {
			$new_session['dir'] = $_DIR;
			$new_session['img'] = array_merge($new_sdata['i'],$new_sdata['v']);
			$session['add_new'] = $new_session;
		}
		
//		echo "<pre>"; print_r( $dir_files ); echo "</pre>";
		return array_merge( $data['i'], $data['v'] );
	}
	
	/** generate thumbnails for a directory
	*
	*/
	function do_dir_thumbs( $db, $_UID, $_DIR, $_P )
	{
		global $session;

		$paths = explode( '/', $_DIR, 2 );
//		echo "thumbs: $_UID, $_DIR<br>";

		$db_files = $this->get_db_files( $db, 'dir', $_UID, $paths[1]
   			, $session['uimg_size'], $_P );
//		echo "<pre>"; print_r( $db_files ); echo "</pre>";
		
		// scan dir for new files
		if ( $session['userid'] == $_UID ) {
			$dir_files = $this->get_dir_files( $_DIR, ".jpg.avi.mpg.mpeg.qt");
			$dir_files = $this->find_new_files( $db, $_UID, $_DIR, $dir_files );
		}
//		echo "<pre>"; print_r( $dir_files ); echo "</pre>";
		
		$this->assign( array(
			'i_thumbs' => $db_files['i'],
			'i_num'    => count($db_files['i']),
			'v_thumbs' => $db_files['v'],
			'v_num'    => count($db_files['v']),
			'p_thumbs' => $db_files['p'],
			'p_num'    => count($db_files['p']),
			'n_thumbs' => $dir_files,
			'n_num'    => count($dir_files),
		 ) );
	}

	/*
	*
	*/
	function do_update_comment( $_uid, $_dbid, $_comment )
	{
		$_comment = addslashes( $_comment );
		
		// only updating one image..
		$sql = "REPLACE image_note SET image_id=$_dbid,user_id=$_uid"
			.",note='$_comment'";

		$this->db->Halt_On_Error = 'no';
		$this->db->query( $sql );
		$this->db->Halt_On_Error = 'yes';
	}

	/**
	*
	*/
	function do_update_access( $db, $_uid, $_iid )
	{
		// update image access flag
		$sql = "UPDATE image set status=NOT status"
			." WHERE id=$_iid AND user_id=$_uid";

		$this->db->Halt_On_Error = 'no';
		$this->db->query( $sql );
		$this->db->Halt_On_Error = 'yes';
	}
	
	/**
	*
	*/
	function do_del_image( $db, $_uid, $_iid )
	{
		$field = 'image_';
		$tables = array( 
			'site_image' => $field,
			'image_note' => $field,
			'image_view' => $field,
			'book_image' => $field,
			'image'      => ''
		) ;
		
		$this->db->Halt_On_Error = 'no';
		
		// Delete image from catalog
		while ( list($k,$v) = each( $tables) ) {
			$sql = "DELETE FROM $k WHERE ".$v."id=$_iid";
//			echo "sql: $sql<br>";
			$this->db->query( $sql );
		}
		$this->db->Halt_On_Error = 'yes';
	}
}

function View_dir_click( $id )
{
	global $session;
	
	return "location.href='?".enc64($session['refurl']."&dir=".urlencode($id))."'";
}
	
include_once( '_js_tree.php' );
include_once( '_image_util.php' );

