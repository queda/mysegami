<?php # $Id: ViewGraph.cl.php,v 1.6 2004-05-21 03:56:36 paulmcav Exp $

IncludeObject('.','db_mysql');

/** ViewGraph page
*
*/
class ViewGraph extends Smarty
{
	var $name = "ViewGraph";
	var $title = "MySegami ViewGraph";
	var $db;
	var $config;

	function ViewGraph()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;
		$this->assign( array(
			'page_title' => $this->title,
			'refurl' => '?'.enc64("page=".$this->name),
			)
	   	);

		$_vm  = $_REQUEST['vm'];
		$_uid = $_REQUEST['uid'];

		if ( $_uid != '' ) { $session['view']['uid'] = $_uid; }
		$_uid = $session['view']['uid'];

		$this->get_images_graph( $this->db, $_vm, $_uid );

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------

	function get_images_graph( $db, $vmode, $user_id ) {

		if ( $vmode >0 ){ $grp = ',3'; } else { $grp = ''; }
		
		$sql = "SELECT COUNT(*),DATE_FORMAT(i.cd,'%Y'),DATE_FORMAT(i.cd,'%m')"
			.",dir"
			." FROM image i LEFT JOIN site_image si ON i.id=si.image_id"
			." WHERE status='a' and i.user_id=$user_id"
			." GROUP BY 2$grp ORDER BY 2 desc,3 desc";

//		echo "sql: $sql<br>";
		$db->query( $sql );

		$cnt = 0;
		$max = 0;
		$oy = '';
		$yc = 0;
		$mc = 0;
		while ( $db->next_record() )
		{
			$row = $db->Record;

			$img_graph[] = array(
				'cnt' => $row[0],
				'_cd' => $row[1].'-'.$row[2],
				'yr'  => $row[1],
				'mo'  => $row[2],
				'url' => $user_id.$row[3],
			);
			if ( substr($row[1],-2) != $oy ) {
				if ( $yc ) {
					$img_year[$yc-1]['cnt'] = $mc+1;
					if ( $grp!='' && $mc<3 ) {
						$img_year[$yc-1]['yr'] =
							substr( $img_year[$yc-1]['yr'], -2 );
					}
				}
				$oy = substr($row[1],-2);
				$img_year[] = array(
				   	'yr' => $row[1],
					'bgclr' => ($yc%2) ? "" : "#d0d0d0",
				);
				$yc++;
				$mc = 0;
			}
			else { $mc++; }
			$cnt++;
			if ( $row[0] > $max ){ $max = $row[0]; }
		}
		$img_year[$yc-1]['cnt'] = $mc+1;

		$width = (int)(625 / $cnt);
		if ( $width > 80 ){ $width = 80; }
//		$width = 13; # 48
//		$width = 79; # 9
//		echo "<pre>cnt: $cnt, wid: $width, max: $max\n";

		for( $i=0; $i<$cnt; $i++ ) {
			$scale = (int)($img_graph[$i]['cnt'] / $max * 26) + 9;
//			echo "i:$i, cnt:".$img_graph[$i]['cnt']." ptc: $scale\n";

			$img_graph[$i]['height'] = $scale;
		}
//		echo "</pre>";

		$this->assign( array(
			'view_mode' => $vmode,
			'img_graph' => $img_graph,
			'img_year'  => $img_year,
			'img_graph_width' => $width,
			)
	   	);
	}
}

