<?php # $Id: EditInfo.cl.php,v 1.4 2004-05-27 06:56:11 paulmcav Exp $

IncludeObject('.','db_mysql');

/** EditInfo page
*
*/
class EditInfo extends Smarty
{
	var $name = "EditInfo";
	var $title = "View Exif: ";
	var $db;
	var $config;

	function EditInfo()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		$_userid = $session['userid'];

		if ( $_userid == '' ){ return; }

		$_exif = $session['exif_data'];

		$_dbid = $_exif['dbid'];
		
		$base = substr($_exif['name'],0,-4);
		$path = strstr( $_exif['path'], '/' );
		
		if ( $_REQUEST['cmd'] == "Update" ){ 
			$this->do_image_update( $this->db, $_REQUEST, $_userid, $_dbid );
			$_js_onload = 'reload_parent();';
		}

		$_img_data = $this->do_get_image_info( $this->db, $_exif['dbid'] );
		
//		echo "<pre>"; print_r( $ex_data ); echo "</pre>";
		
		$this->assign( array(
			'page_title' => $this->title.$base,
			'refurl'     => '?'.enc64('page='.$this->name),
			'image'      => $_img_data,
			'base_name'  => $base,
			'base_path'  => $path,
			'js_onload'  => $_js_onload,
			)
	   	);

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------

	/**
	*
	*/
	function do_get_image_info( $db, $_DBID )
	{
		global $session;

		$sql = "SELECT * FROM image WHERE "
			."id=$_DBID AND user_id=".$session['userid'];

		$db->query( $sql );

		if( $db->next_record() ) {
			$row = $db->Record;
//			echo "<pre>"; print_r( $row ); echo "</pre>";

			$media = "select_".$row['media'];

			$info = array(
				'id'     => $row['id'],
				'name'   => $row['name'],
				'dir'    => $row['dir'],
				'status' => $row['status'],
				'media'  => $row['media'],
				'title'  => $row['title'],
				'notes'  => $row['notes'],
				'kwds'   => $row['kwds'],
				'views'  => $row['views'],
				'xtra'   => $row['xtra'],
				$media   => 'checked',
			);
		}
		return $info;
	}

	/**
	*
	*/
	function do_image_update( $db, $req, $userid, $dbid )
	{
		// update currently displayed image, else use id from form
		if ( !$dbid ) { $dbid = $req['f_id']; }
		
		
		
		$sql = "UPDATE image SET"
			." title='".addslashes($req['f_title'])."'"
			.",notes='".addslashes($req['f_notes'])."'"
			.",kwds='".strtolower(addslashes($req['f_kwds']))."'"
			.",media=IF(media!='v','".$req['f_media']."',media)"
			." WHERE id=".$dbid
			." AND user_id=$userid";

		$db->query( $sql );
	}
}

