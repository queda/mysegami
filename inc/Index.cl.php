<?php # $Id: Index.cl.php,v 1.14 2004-06-02 00:39:09 paulmcav Exp $

IncludeObject('.','db_mysql');

/** Index page
*
*/
class Index extends Smarty
{
	var $name = "Index";
	var $title = "MySegami Index";
	var $db;
	var $config;

	function Index()
	{
		global $session; 
	
		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		$this->assign( array(
			'page_title' => $this->title,
			'refurl'     => "?".enc64($session['refurl']),
			'head_title' => 'Images To Share..',
			));

//		if ( $_REQUEST['x'] == -1 ){ $this->remap_images( $this->db ); }

		// -- deal with setting viewed image size
		$this->assign( set_user_image_size( $this, $_REQUEST['is'] ) );
//		$this->assign( 'v_size_select','1' );
		
		// if the user is not in the list, then show how to add new items.
		if ( $this->get_userlist( $this->db, $_REQUEST['uid'] ) ) {
			// show add new items link
			$this->assign( "new_items", '1' );
		}

		if ( $_REQUEST['uid'] > 0 ) {
			$this->select_user_collections( $this->db, $_REQUEST['uid'] );
		}
		else {
			$this->select_rand_image( $this->db );
			$this->assign( "page_content", 'rand_image' );
		}

//		if ( $this->is_cached( $this->name.".html" ) ) { echo "cached.<br>"; }

		unset( $session['view'] );
		unset( $session['exif_data'] );
		unset( $session['viewslide'] );
		unset( $session['add_new'] );

		// final process... output page
		$out = $this->fetch( $this->name.".html" );
		$this->assign( "body", $out );
		$this->display( "common.html" );
	}

	// ----------------------
	function get_userlist( $db, $li_uid )
	{
		global $session; 

		$sql = "SELECT COUNT(*) as img_cnt,u.id,u.name,email FROM image i"
			  ." LEFT JOIN user u ON u.id=i.user_id"
			  ." LEFT JOIN site_image si ON i.id=si.image_id"
			  ." WHERE status='a'"
//			  ." AND si.server_id=".$session['srv_id']
//			  ." AND si.server_id=2"
			  ." GROUP BY u.id";
	
		$db->query( $sql );

		$add_new_files = 1;
		
		while ( $db->next_record() )
		{
			$row = $db->Record;
			
			// User already has some images listed.
			if ( $row['id'] == $session['userid'] ) {
				$add_new_files = 0;
			}
			$userlist[] = array(
				"userlink" =>
					"?".enc64("page=".$this->name."&uid=".$row['id'] ),
				"img_cnt"  => $row['img_cnt'],
				"id"       => $row['id'],
				"name"     => $row['name'],
				"email"    => $row['email'],
				);

			// looking at this specific user
			if ( $li_uid == $row['id'] ) {
				$this->assign( array( 
					'collection_user' => $row['name'],
					'collection_cnt'  => $row['img_cnt'],
				) );
			}
		}

		// look for possible files in user dir..
		if ( $add_new_files ) {
			$this->assign( array( 
				"add_new_files" => $session['userid'],
		   		"add_new_url"   =>
					'?'.enc64("page=View&uid=".$session['userid']),
				));
		}
		
		$this->assign( 'user_list', $userlist );

//		echo "<pre>"; print_r( $userlist ); echo "</pre>";
	}
	
	function select_rand_image( $db )
	{
		global $session;

		$sql = "SELECT COUNT(*) cnt,SUM(RAND()),iv.image_id"
			." FROM image_view iv LEFT JOIN site_image si"
			." ON si.image_id=iv.image_id"
			." LEFT JOIN image i ON si.image_id=i.id"
			." WHERE "
//			." si.server_id=".$session['srv_id']." AND"
			." si.server_id=2 AND"
			." i.status='a' AND i.media='i'"
			." GROUP BY iv.image_id HAVING cnt>3  ORDER by 1";

//		echo "sql: $sql<br>";
		$db->query( $sql );
		
		if ( ($nr=$db->num_rows()) ) {
				$nr = rand(1,$nr);
				
				$db->seek($nr);
				$db->next_record();
				$row = $db->Record;
				$views = $row['cnt']." Views";

//				$row['image_id'] = 8649;
				
				$sql = "SELECT *,i.id iid,DATE_FORMAT(cd,'%d%b%y %H:%i') _cd"
					." FROM image i"
					." LEFT JOIN site_image si ON i.id=si.image_id"
					." WHERE i.id=".$row['image_id'];
//				echo "sql: $sql<br>";
				$db->query( $sql );
				
				$db->next_record();
				$row = $db->Record;

				$dims = explode('x',$row['res']);
				$_Image_Rez = cfg_to_array( $this->config, 'size' );
				$i_size = 5;

				$iscale = @min($_Image_Rez[$i_size]/$dims[0],
								$_Image_Rez[$i_size]/$dims[1]);

				$iw = (int)($dims[0]*$iscale);
				$ih = (int)($dims[1]*$iscale);
				
//	echo "iw: $iw, ih: $ih<br>";
				if ( $iw == 0 ) { $iw = 10; $ih = 10; }
				
				$img_dim="width=\"$iw\" height=\"$ih\"";
#					$img_src="media.php?".enc64("s=0&f=".$row['user_id']
#						.$row['dir']."/.thumbnails/".$row['name'].".png");
				$img_src="media.php?".enc64("s=$i_size&dbid=".$row['iid']);

//	echo "img_src: $img_src<br>";
				
//				$bid = substr($row['dir'],0,strrpos($row['dir'],'/'));
//				$ds  = strrchr($row['dir'],'/');
//				$ds = substr($ds,1);
				
				$img_url = "page=View&dir=".$row['user_id'].$row['dir'];
				if ( $session['userid'] != '' ) {
					$img_url .= "&dbid=".$row['iid'];
				}

				$img_url = "?".enc64( $img_url );
				
//	echo "dir: $row[dir]<br>";

				$title = $row['title'];
				$name = substr($row['name'],0,-4);
				if ($title=='') {
					$title = $name;
					$name = '';
				}
				else {
					$name .= ": ";
				}
				$_cd = $row['_cd'];
				$notes = $row['notes'];

//				$this->t->set_var($row);
		}
		else {
			$title = '';
			$_cd = '';
			$img_url = '';
			$img_src = 'images/iris_40.png';
			$img_dim = '';
			$views = '';
		}
		$this->assign( array(
				'title'    => $title,
				'notes'    => $notes,
				'date'     => $_cd,
				'name'     => $name,
				"img_url"  => $img_url,
				"img_src"  => $img_src,
				"img_size" => $img_dim,
				"views"    => $views,
			) );
	}

	function select_user_collections( $db, $user_id )
	{

		$this->get_collections( $db, $user_id );
		
		$_usr_img_link = "?".enc64("page=View&uid=$user_id" );

		$this->assign( array(
			'user_img_link'   => $_usr_img_link,
			'' => '',
		) );
	}


	function get_collections( $db, $user_id )
	{
		$b_status = "1";
		$srv_id = 1;

		$sql = "SELECT b.*,"
			  ."SUM(IF(bi.image_id is not null,1,0)) AS img_cnt"
			  .",DATE_FORMAT(b.cd,'%d%b%y') AS _cd,si.server_id"
			  ." FROM book b"
			  ." LEFT JOIN book_image bi ON b.id=bi.book_id"
			  ." LEFT JOIN site_image si ON bi.image_id=si.image_id"
			  ." WHERE $b_status AND b.user_id=$user_id"
//			  . " AND b.id in (58,48) or b.p_id in (48,58) "
#				  ." AND si.server_id=".$session['srv_id']
			  ." GROUP BY b.id"
//			  ." HAVING (si.server_id IS NULL OR si.server_id=$srv_id)"
			  ." ORDER BY b.cd";

//echo "sql: $sql<br>";

		$this->db->query( $sql );
		while ( $this->db->next_record() ) {
			$row = $this->db->Record;

			$node = array(
				'p_id' => $row['p_id'],
				'id'   => $row['id'],
				'name' => $row['name'],
				'_cd'  => $row['_cd'],
//				'url' => '?'.enc64("#"),
				'img_cnt' => $row['img_cnt'],
				'image_url' => enc64("s=5&dbid=".$row['image_id']),
				'clctn_url' => enc64( "page=View&bkid=".$row['id'] ),
				'notes' => $row['notes'],
			);

			$list[$row['p_id']][] = $node;
		}

		$list = jst_get_collections( $db, $user_id );
//		echo "<pre>"; print_r( $list ); echo "</pre>";

		$book_tree = jst_build_tree( $list );
//		echo "<pre>"; print_r( $book_tree ); echo "</pre>";

		$collection = array();
		jst_build_output( $book_tree, 0, 0, $collection );
//		echo "<pre>"; print_r( $collection ); echo "</pre>";
		
		$this->assign( 'collection',$collection );
	}

	/** move image data (title,notes,views) from one image to another
	*
	*/
	/*
	function remap_images( $db )
	{
		return;
		$data = array();

		$sql = "SELECT * FROM image where dir like '/2003/europe-%'";
		$db->query( $sql );
		
		while ( $db->next_record() )
		{
			$row = $db->Record;
			array_push( $data, $row );
		}
		echo "<pre>";
		while ( $row = array_pop($data ) ) {
			echo "oid: $row[id]";
			echo " ,$row[name]";
			echo " ,$row[dir]";
			echo " ,$row[views]";
			echo "\n";

			$sql0 = "SELECT * from image"
					." WHERE name like '$row[name]' AND id!=$row[id]";
			$db->query( $sql0 );

			if ( ($nr=$db->num_rows()) ) {
				$db->next_record();
				$row2 = $db->Record;

				echo "nid: $row2[id]";
				echo " ,$row2[name]";
				echo " ,$row2[dir]";
				echo " ,$row2[views]";
				echo "\n";

				$sql1 = "UPDATE image set title='".addslashes($row[title])."'"
						.",notes='".addslashes($row[notes])."',views=$row[views]"
						." WHERE id=$row2[id]";
//				echo "sql1: $sql1\n";
				$db->query( $sql1 );
				$sql2 = "UPDATE image_view set image_id=$row2[id]"
						." WHERE image_id=$row[id]";
//				echo "sql2: $sql2\n";
				$db->query( $sql2 );
				$sql3 = "UPDATE image set dir='_$row[dir]' WHERE id=$row[id]";
//				echo "sql3: $sql3\n";
				$db->query( $sql3 );
//			echo "$nsql\n";
			}
				
		}
		echo "</pre>";
	}
*/
}

require_once( '_js_tree.php' );
