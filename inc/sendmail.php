<?php

function sendmail($to, $subject, $message, $headers='', $extra='')
{
	if ( $extra=="") {
		$extra = "-froot@".$_SERVER['HTTP_HOST'];
	}
	$fd = popen("/usr/sbin/sendmail -t $extra", 'w');

	fputs($fd, "To: $to\n");
	fputs($fd, "Subject: $subject\n");
	fputs($fd, "X-Mailer: PHP4\n");

	if ($headers)
		fputs($fd, "$headers\n");

	fputs($fd, "\n");
	fputs($fd, $message);
	pclose($fd);
}

