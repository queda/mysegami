<?php # $Id: ViewExif.cl.php,v 1.4 2004-05-27 00:16:31 paulmcav Exp $

IncludeObject('.','db_mysql');

/** ViewExif page
*
*/
class ViewExif extends Smarty
{
	var $name = "ViewExif";
	var $title = "View Exif: ";
	var $db;
	var $config;

	function ViewExif()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		if ( $session['userid'] == '' ){ return; }

		if ( $_REQUEST['f'] != '' ) {
			$base = $_REQUEST['f'];
			$file = $this->config['img_dir']."/$base";
		}
		else {
			$_exif = $session['exif_data'];
			
			$base = substr($_exif['name'],0,-4);
			$path = strstr( $_exif['path'], '/' );
			
			$file = $this->config['img_dir'].'/'.$_exif['path'].'/';
			if ( $_exif['media'] != 'v' ) {
				$file .= $_exif['name'];
			}
			else {
				$file .= $base.'.thm';
			}
		}

		$exif_data = do_read_exif_data( $file );
		
		foreach( $exif_data as $k => $v ){
			if ( $v!=''  && !is_array($v)) {
			   	$_data[] = array( 
					'key' => $k,
					'val' => $v,
				);
		   	}
		}

//		echo "base,file: $base, $file<br>";
//		echo "<pre>"; print_r( $ex_data ); echo "</pre>";
		
		$this->assign( array(
			'page_title' => $this->title.$base,
			'exif_data'  => $_data,
			'base_name'  => $base,
			'base_path'  => $path,
			)
	   	);

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------
}

include_once( '_exif.php' );

