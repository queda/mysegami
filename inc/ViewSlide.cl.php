<?php # $Id: ViewSlide.cl.php,v 1.3 2004-06-02 00:39:09 paulmcav Exp $

IncludeObject('.','db_mysql');

/** ViewSlide page
*
*/
class ViewSlide extends Smarty
{
	var $name = "ViewSlide";
	var $title = "MySegami ViewSlide";
	var $db;
	var $config;

	var $_ImageRez;

	function ViewSlide()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;
		
		if ( $session['userid'] == '' ){ die; }

		$_DIR = $_REQUEST['dir']; 
		if ( $_DIR == '' ){ die; }

		$_I = $_REQUEST['i'];
		if ( $_I == '' ){ $_I = 0; }
		
		$_DLY = $_REQUEST['dly'];
		if ( $_DLY == '' ){ $_DLY = 5; }

		// -----
		
		$this->_Image_Rez = cfg_to_array( $this->config, 'size' );

		$paths = explode( '/',$_DIR,2 );
		
		$slide_data = $session['viewslide'];
		if ( $slide_data['path'] != $_DIR ) {
			$images = $this->do_get_slides( $this->db, $paths[0], $_DIR );
			$slide_data['p_id'] = $_DIR;
			$slide_data['v_id'] = $images;
			$session['viewslide'] = $slide_data;
		}
		else {
			$images = $slide_data['v_id'];
		}
//	echo "<pre>"; print_r( $images ); echo "</pre>";

		if ( !count($images) ) { die; }
	
		$this->do_show_image( $paths[0], $_DIR, $images, $_I );

		$this->assign( array(
			'page_title' => $this->title.': '.$paths[1],
			'refurl' => '?'.enc64('page='.$this->name),
			'base_path' => $paths[1],
			$_DLY."_dly" => "selected",
			)
	   	);

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------

	function do_get_slides( $db, $_UID, $_DIR )
	{
		global $session;

		
		// only owner can see non 'a'vailable records
		if ( $session['userid'] != $_UID ) {
			$status .= "AND status='a'";
		}

		$paths = explode( '/', $_DIR, 2 );
		
//		if ( $session['view']['p_id'] != $_DIR ) {
			$sql = "SELECT i.name,i.id iid,si.id siid"
				." FROM image i"
				." LEFT JOIN site_image si ON i.id=si.image_id"
				." WHERE i.user_id=$_UID"
	//			." AND si.server_id=".$session['srv_id']
				." AND i.dir='/".addslashes($paths[1])."' $status"
				." AND i.media!='v'"
				." ORDER BY media,cd,name";

			$db->query( $sql );
			while ( $db->next_record() ) {
				$id_list[] = $db->Record['iid'];
			}

//			$session['view']['p_id'] = $_DIR;
//			$session['view']['v_id'] = $id_list;
//		}
//		else {
//			$id_list = $session['view']['v_id'];
//		}

		// scan list of id's to find next / prev images
		$i_num = 0;
		$i_max = count($id_list);

		if ( !$i_max ){ return; }

		for ( $i_cnt = 0; $i_cnt < $i_max; $i_cnt++ ) {
//			if ( $id_list[$i_cnt] == $_DBID ){ $i_num = $i_cnt; }
			$in_list .= ",".$id_list[$i_cnt];
		}
//		$i_prv = $i_num-1; 
//		$i_nxt = $i_num+1;
//	echo "p,c,n: $i_prv, $i_num, $i_nxt, $i_max<br>";
//	echo "<pre>"; print_r( $id_list ); echo "</pre>";
		
//		$size = $session['uimg_size'];
//		$scale = $this->_Image_Rez[ $size ];
		
		// select requested records
		$sql = "SELECT i.id iid,si.*,si.id siid"
			." FROM image i"
			." LEFT JOIN site_image si ON i.id=si.image_id"
			." WHERE i.user_id=$_UID"
//			." AND si.server_id=".$session['srv_id']
			." AND i.id in(0$in_list) $status"
			." ORDER BY media,cd,name";
//		echo "sql: $sql<br>";
		$db->query( $sql );
		while ( $db->next_record() ){

			$images[] = $row = $db->Record['iid'];
		}
		return $images;
	}

	function do_show_image( $_UID, $_DIR, $image_list, $num=0 )
	{
		global $session;

		$size = $session['uimg_size'];
		
		$i_cnt = count($image_list);
		$position = ($num+1)."/".($i_cnt);

		$img_data = get_image_data( $this, $_UID, $image_list[$num], $size );

		$img_nav['prv_img'] = '#';
		
		$prv_url = "#";
		
		if ( ++$num >= $i_cnt ){
		   	$num = -1;
			$nxt_url = "#";
	   	}
		else {
			$nxt_url = enc64("page=ViewSlide&dir=$_DIR&i=$num");
			$img_nav['nxt_img'] = enc64("g=1&s=$size&dbid="
				.$image_list[$num]);
		}
		
//		$image = $image_list[$num];

		$this->assign( array(
			'image'   => array_merge($img_data[0],$img_nav),
			'comment' => $img_data[1],
			'position' => $position,
			'nxt_num' => $num,
			'prv_url' => $prv_url,
			'nxt_url' => $nxt_url,
		) );
		return;
	}

}

include_once( '_image_util.php' );	

