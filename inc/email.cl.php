<?php # $Id: email.cl.php,v 1.2 2004-04-30 15:13:39 paulmcav Exp $

//IncludeObject('Template');

/** generic class to handle templated email messages.
*
*/
class email extends smarty
{
	var $name;

	function email( $_name, $policy='remove' )
	{

		$this->name = $_name;

		$this->compile_check = true;
	}

	function send($subj, $to, $from='', $headers = array())
	{
		global $session;

		if ( $from=='' ) {
			$from = "root@".$_SERVER['HTTP_HOST'];
		}
		$this->assign( array(
			'Subject' => $subj,
			'To'      => $to,
			'From'    => $from,
			) );
		$this->assign( $headers );

		$msg = $this->fetch( $this->name.'.txt' );
		
		$_headers = "From: $from";
		while( is_array($headers) && list($k,$v) = each($headers) )
		{
			if ( $_headers != '' ){ $_eaders .= "\r\n"; }
			$_headers .= "$k: $v";
		}
		
//		echo "<pre>$msg</pre>";
		mail( $to, $subj, $msg, $_headers, "-f$from" );
	}
}

#include_once( 'sendmail.php' );

