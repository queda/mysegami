<?	# $Id: _image_util.php,v 1.1 2004-06-10 02:00:38 paulmcav Exp $


/** Given a row of data, generate output image info
*
*/
function make_image_info( $this, $row, $det, $scale, $size )
{
	global $session;

	$tscale = $this->_Image_Rez[ $row['media'] ];

	// calc image dimensions
	$src_size = explode(' x ',$row['res']);

	$tscale = @min($tscale/$src_size[0], $tscale/$src_size[1]);
	if ( !$tscale ) { $tscale = 1; }

	$dst_tw = intval($src_size[0]*$tscale);
	$dst_th = intval($src_size[1]*$tscale);

	if ( !$dst_tw ) {
		if ( $row['media'] == 'v' ) {
			$dst_tw = 160; $dst_th = 120; 
		}
		else {
//				$dst_tw = 85; $dst_th = 85; 
		}
	}

	$tscale = @min($scale/$src_size[0], $scale/$src_size[1]);
	if ( !$tscale ) { $tscale = 1; }

	$dst_w = intval($src_size[0]*$tscale);
	$dst_h = intval($src_size[1]*$tscale);

	$_views = $row['views'];
	
	if ( $session['userid'] != 0 ) {
		// looking at image details
		if ( $det ) {
			$src_url = "s=$size&dbid=".$row['iid'];
			$dld_url = "media.php?".enc64($src_url."&dl=1");
			$src_url = 'media.php?'.enc64($src_url);
			
			// url for viewing pano image
			$_pano_url = enc64("page=ViewPano&dbid=".$row['iid']);
		}
		else {
			$src_url = 
	"location.href='?".enc64($session['refurl']."&dbid=".$row['iid'])."';";
		}
//			"open_img('media.php?".enc64("s=$size&dbid=".$row['iid'])."');";
//			"open_win('media.php?".enc64("s=$size&dbid=".$row['iid'])."','photo','820','620');";

		// stuff to add if user owns image
		if ( $session['userid'] == $row['user_id'] ) {
			$_owner = 1;
			$_edit_url = enc64("page=EditInfo&dbid=".$row['iid']);
			$_views = '<a href="#" onClick="ViewViews=open_win(\'?'
			.enc64('page=ViewViews&id='.$row['iid'].'|'.$row['user_id'].$row['dir']
				.'|'.$row['name'])."'"
				.',\'ViewViews\',420,400);return false;">'.$row['views'].'</a>';
		}
	}
	else {
		$src_url = "need_login();";
	}

	$name = substr( $row['name'], 0,strrpos( $row['name'],'.' ) );
//		if ( $session['userid'] == $row['user_id'] && $row['status'] != 'a' ){
//			$name = "<strike>$name</strike>"; }

//		if ( $row['title'] != '' ) { $name = "<b>$name</b>"; }
	
	$data = array(
		'server_id' => $row['server_id'],
		'iid'       => $row['iid'],
		'siid'      => $row['ssid'],
		'fname'     => $row['name'],
		'name'      => htmlspecialchars($name),
		'title'     => htmlspecialchars($row['title']),
		'nicedate'  => $row['nicedate'],
		'img_thm'   => enc64("s=t&dbid=".$row['iid']),
		'img_tx'    => $dst_tw,
		'img_ty'    => $dst_th,
		'img_src'   => $src_url,
		'img_x'     => $dst_w,
		'img_y'     => $dst_h,
		'status'    => $row['status'],
		'views'     => $row['views'],
		'f_title'   => ($row['title'] ? 't' : ''),
		'num_views' => ($row['views'] ? "($_views)" : ''),
		'owner'     => $_owner,
		// status
		// priv
		// xtra
	);

	// extra information when requesting detailed data
	if ( $det ) {
		
		$data = array_merge( $data, array(
			
		'dir'       => htmlspecialchars($row['dir']),
		'notes'     => htmlspecialchars($row['notes']),
		'kwds'      => htmlspecialchars($row['kwds']),
		'res'       => $row['res'],
		'size'      => $row['size'],
		'kbytes'    => sprintf( "%.1f", ($row['size'] / 1000)),
		'media'     => $row['media'],
		'cd'        => $row['cd'],
		'exif_url'  => enc64('page=ViewExif'),
		'dld_url'   => $dld_url,
		'edit_url'  => $_edit_url,
		'pano_url'  => ($row['media']=='p' ? $_pano_url : ''),
			) );

		$exif_data = array(
			'dbid'  => $row['iid'],
			'name'  => $row['name'],
			'path'  => $row['user_id'].$row['dir'],
			'media' => $row['media'],
		);
		$session['exif_data'] = $exif_data;
	}
//			$file_list[] = $row;

	return $data;
}
	
/*
*
*/
function get_image_data( $this, $_UID, $_DBID, $size )
{
	global $session;
	$db = $this->db;

	// only owner can see non 'a'vailable records
	if ( $session['userid'] != $_UID ) {
		$status = "AND status='a'";
	}
	
	// select requested records
	$sql = "SELECT i.*,i.id iid,si.*,si.id siid,"
		." DATE_FORMAT(cd,'%d%b%y %H:%i') nicedate"
		." FROM image i"
		." LEFT JOIN site_image si ON i.id=si.image_id"
		." WHERE "
//		." i.user_id=$_UID AND"
		." si.server_id=2"
//			." AND si.server_id=".$session['srv_id']
		." AND i.id=$_DBID $status";

	$scale = $this->_Image_Rez[ $size ];

	$db->query( $sql );
	if ( $db->next_record() ){
		$row = $db->Record;
//		$data = $this->make_image_info( $this, $row, 1, $scale, $size );
		$data = make_image_info( $this, $row, 1, $scale, $size );
//			echo "<pre>"; print_r( $data ); echo "</pre>";
	
		// get user notes
		$sql = "SELECT u.*,user_id,note,n.ts"
			." FROM image_note n LEFT JOIN"
			." user u ON n.user_id=u.id WHERE image_id=$_DBID";
#	echo "sql: $sql<br>";			
		$this->db->query( $sql );
		
		$db->query( $sql );
		while ( $db->next_record() ) {
			$row = $db->Record;
//			echo "<pre>"; print_r( $row ); echo "</pre>";

			// skip blank notes
			if ( $row['note'] == '' ) { continue; }
			$comment[] = array(
				'email' => $row['email'],
				'name'  => $row['name'],
				'note'  => $row['note'],
				'uid'   => $row['id'],
				'cd'    => $row['ts'],
			);

			// track users own comment for the html form
			if ( $session['userid'] == $row['user_id'] ) {
				$user_comment = $row['note'];
			}
		}
	}
	
	return array(
		$data,
		$comment,
		$user_comment,
	);
}
