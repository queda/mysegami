<?php # $Id: ViewPano.cl.php,v 1.2 2004-06-10 02:00:38 paulmcav Exp $

IncludeObject('.','db_mysql');

/** ViewPano page
*
*/
class ViewPano extends Smarty
{
	var $name = "ViewPano";
	var $title = "View Pano: ";
	var $db;
	var $config;

	function ViewPano()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		if ( $session['userid'] == '' ){ return; }
		
		$_DBID = $_REQUEST['dbid'];

		if ( $_DBID == '' ) { return; }
		
		// -----
		
		$this->_Image_Rez = cfg_to_array( $this->config, 'size' );

//		$size = $session['uimg_size'];
		$size = 0;

		$img_data = get_image_data( $this, $_UID, $_DBID, $size );

//		echo "<pre>"; print_r( $img_data ); echo "</pre>";
		
		$this->assign( array(
			'page_title' => $this->title,
			'image'      => $img_data[0],
			)
	   	);

		// final process... output page
//		$out = $this->fetch( $this->name.".html" );
//		$this->assign( "body", $out );
//		$this->display( "common.html" );
		$this->display( $this->name.".html" );
	}

	// ----------------------
}

include_once( '_image_util.php' );

