<?php # $Id: Register.cl.php,v 1.6 2004-05-28 02:21:50 paulmcav Exp $

IncludeObject('.','db_mysql');
IncludeObject('.','email');

/** Register page
*
*/
class Register extends Smarty
{
	var $name = "Register";
	var $title = "MySegami Register";
	var $db;
	var $config;

	function Register()
	{
		global $session; 
	
//		$session['refurl'] = "page=".$this->name;

		$this->db = new db();
	}

	function main()
	{
		global $session; 

//		$this->debugging = true;

		$this->assign( array(
			'page_title' => $this->title,
			'refurl'     => enc64( "page=Register" ),
			'head_title' => 'Account Registration',
			)
	   	);

		$_CMD    = $_REQUEST['cmd'];
		$_email  = $_REQUEST['email'];
		$_email2 = $_REQUEST['email2'];
		$_part   = $_REQUEST['part'];

		// always start in part1 mode if none given
		if ( $_part == '' ) { $_part = 1; }
		
		if ( !is_array($session['req']) && $_part > 1 ) { $_part=1; $_CMD=''; }
		
		if ( $_CMD == 'Submit' ) {
			// part 1
			if ( $_part == 1 ) {
				
				$ret = FindUser( $this->db, $_email, 'mysecret',$row );

//				echo "ret: $ret<pre>"; print_r( $row ); echo"</pre>";
				
				// user id was found..
				if ( $ret!=-1 ) {
					$_mode = 'uid_found';
					$_send_email = enc64("page=Login&cmd=2&uid=$_email");
				}
				// no user was found, continue registration process
				else {
					$_part = 2;
				}
				$this->assign( 'email', $_email );
				$session['req']['email'] = $_email;
			}
			// part 2
			else if ( $_part == 2 ) {

				$req = $_REQUEST;
				$req['email'] = $session['req']['email'];
				
				$missing = array( 'email1','name','pass','pass1');

				while ( list($k,$v)=each( $missing ) ){
//					echo "v: $v<br>";
					if ( $req[$v] == '' ) {
						if ( $_mode == '' ) { $_mode = 'msg_'.$v; };
						$req['err_'.$v] = "err_message";
					}
				}
				
				// mismatch email
				if ( $req['email'] != $req['email1'] ) {
					$_mode = 'bad_email';
				}
				else if ( $req['pass'] != $req['pass1'] ) {
					$_mode = 'bad_pass';
					$req['err_pass1'] = 'err_message';
				}
				// redisplay registration page
				if ( $_mode != '' ) {
					$_part = 2;
					$this->assign( $req );
				}
				// everything seems to be okay... register the user.
				else {
					$_part = 3;
					$uid = $this->register_user( $this->db, $req );

					if ( $uid != 0 ) {
						$this->send_password( $uid );

						ValidateUser($this->db, $obj->config, $uid, $req['pass'] );

						$loc = $session['refurl'];
						if ( $loc == '' ){ $loc = "page=Index"; }

						$this->assign( 'prev_url', $loc );
//						unset( $session['req'] );
					}
				}
			}
//			echo "<pre>"; print_r( $req ); echo "</pre>";
		}

//		$session['reg']['part'] = $_part;


		$this->assign( array(
			'r_mode'     => $_mode,
			'part_num'   => $_part,
			'send_email' => $_send_email,
		) );
		
		// final process... output page
		$out = $this->fetch( $this->name.".html" );
		$this->assign( "body", $out );
		$this->display( "common.html" );
	}

	// ----------------------

	function send_password( $uid )
	{
		$row = FindUser( $this->db, $uid );

		if ( !is_array( $row ) )
		{
			return 0;
		}
		
		// try and send email w/ the password to the found user.
		$email = new email('msg_subscribe');

		$email->assign( array(
			'name'  => $row['name'],
			'email' => $row['email'],
			'pass'  => $row['passwd'],
			'uid'   => $row['id'],
//			'h_url' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
			'h_url' => $_SERVER['HTTP_HOST'].'/?page=Index',
			) );

		$email->send(
			$this->title.' Account Form',
//			'paulmcav',
			$row['email'],
			'paulmcav@queda.net'
			);
		return 1; 
	}

	function valid_email( $email )
	{
		if (eregi("^[^@[:space:]]+@([[:alnum:]\-]+\.)+[[:alnum:]][[:alnum:]][[:alnum:]]?$", $email) )
		{
			return 1;
		}
		return 0;
	}
   
	function register_user( $db, $req )
	{
		$sql = "INSERT INTO user "
			  ."(email,passwd,name,address,city,state,zip,phone,cd) "
			  ."VALUES('".$req['email']."'"
			  .",'".$req['pass']."'"
			  .",'".$req['name']."'"
			  .",'".$req['address']."'"
			  .",'".$req['city']."'"
			  .",'".$req['state']."'"
			  .",'".$req['zip']."'"
			  .",'".$req['phone']."'"
			  .",now()"
			  .")";

		$db->query( $sql );
		
		$uid = $db->get_last_insert_id('user','id');

		return $uid;
	}

   
		
}

