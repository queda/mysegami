// $Id: scripts.js,v 1.16 2004-06-02 00:39:09 paulmcav Exp $

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function mail(here) {
        here = here.replace(/\s+at\s+/, '@');
        here = here.replace(/\s+dot\s+/, '.');
        location.href = "mailto:" + here;
}

function open_win( url, name, w, h, xtra ) {
 w = parseInt( w );
 h = parseInt( h ) ;
 id=window.open(''+url,''+name,'resizable=1,width='+w+',height='+h+',scrollbars=1'+xtra);
// win.focus(); return false;
 return id;
}

function need_login() {
	alert('Please login to view large images.');
}

function open_me( url ) {
	location.href = url;
}

function open_img( url ) {
	var w_tn = document.getElementById( 'view_thumb' );
	var w_im = document.getElementById( 'view_image' );
	var img = document.getElementById( 'v_img' );

	img.src = '/images/iris_40.png';

	w_tn.style.display = "none";
	w_im.style.display = "block";

	img.src = url;
}

function close_img() {
	var w_tn = document.getElementById( 'view_thumb' );
	var w_im = document.getElementById( 'view_image' );

	w_tn.style.display = "block";
	w_im.style.display = "none";
}

function comment_edit() {
	var c_ed = document.getElementById( 'comment_edit' );
	var c_fm = document.getElementById( 'comment_form' );

	c_ed.style.display = "none";
	c_fm.style.display = "block";
}

function check_all(fName) {
	var f = document.forms[fName];
	for (var i=0; i< f.elements.length; i++) {
	  if (f.elements[i].type == 'checkbox'){
		f.elements[i].checked = !(f.elements[i].checked);
	  }
	}
}

function reload_parent( id ) {
	window.opener.location.reload();
}

// need to define these vars for winHandles
var EditInfo;
var ViewExif;

function urlppup( url ) {
	var win = 0;
	if ( window.EditInfo && !window.EditInfo.closed ) { win |= 1; }
	if ( window.ViewExif && !window.ViewExif.closed ) { win |= 2; }

	url = url.substring(1);
//	url = decode64(url);

	url += "&wJSOL="+win;
//	alert( "urlppup: "+encode64(url) );
	location.href = "?"+url;

var frm = top.frames;
for (i=0; i < frm.length; i++)
    alert('frm['+i+'] '+frm[i].location);

	return false;
}

function urlopen( id, varname) {
	var url = document.getElementById( id );
	return open_win( url.href, id, 320,300 );
}

