
function findChildNode(node, name) 
{
  var temp;
  if (node == null) 
  {
	return null;
  } 
  node = node.firstChild;
  while (node != null) 
  {
	if (node.nodeName == name) 
	{
	  return node;
	}
	temp = findChildNode(node, name);
	if (temp != null) 
	{
	  return temp;
	}
	node = node.nextSibling;
  }
  return null;
}

function toggleFolder(id, imageNode) 
{
  var folder = document.getElementById(id);
  var l = 0;
  var vl = "images/ftv2vertline.png";
  if (imageNode != null && imageNode.nodeName != "IMG") 
  {
	imageNode = findChildNode(imageNode, "IMG");
	if (imageNode!=null) l = imageNode.src.length;
  }
  if (folder == null) 
  {
  } 
  else if (folder.style.display == "block") 
  {
	while (imageNode != null && 
		   imageNode.src.substring(l-vl.length,l) == vl)
	{
	  imageNode = imageNode.nextSibling;
	  l = imageNode.src.length;
	}
	if (imageNode != null) 
	{
	  l = imageNode.src.length;
	  imageNode.nextSibling.src = "images/ftv2folderclosed.png";
	  if (imageNode.src.substring(l-13,l) == "ftv2mnode.png")
	  {
		imageNode.src = "images/ftv2pnode.png";
	  }
	  else if (imageNode.src.substring(l-17,l) == "ftv2mlastnode.png")
	  {
		imageNode.src = "images/ftv2plastnode.png";
	  }
	}
	folder.style.display = "none";
  } 
  else 
  {
	while (imageNode != null && 
		   imageNode.src.substring(l-vl.length,l) == vl)
	{
	  imageNode = imageNode.nextSibling;
	  l = imageNode.src.length;
	}
	if (imageNode != null) 
	{
	  l = imageNode.src.length;
	  imageNode.nextSibling.src = "images/ftv2folderopen.png";
	  if (imageNode.src.substring(l-13,l) == "ftv2pnode.png")
	  {
		imageNode.src = "images/ftv2mnode.png";
	  }
	  else if (imageNode.src.substring(l-17,l) == "ftv2plastnode.png")
	  {
		imageNode.src = "images/ftv2mlastnode.png";
	  }
	}
	folder.style.display = "block";
  }
}

function openFolders(path)
{
	var i;
	var l = path.length;
	var f;

	for( i=l; i>0; i--){
		if ( path.charAt(i) == "/") {
			f = path.substring(0,i);
			openFolder( f );
		}
	}
}

function openFolder(id) 
{
	var imageNode = document.getElementById(id);
	var folder    = document.getElementById( 'folder'+id );
	
	if (imageNode != null) 
	{
	  l = imageNode.src.length;
	  imageNode.nextSibling.src = "images/ftv2folderopen.png";
	  if (imageNode.src.substring(l-13,l) == "ftv2pnode.png")
	  {
		imageNode.src = "images/ftv2mnode.png";
	  }
	  else if (imageNode.src.substring(l-17,l) == "ftv2plastnode.png")
	  {
		imageNode.src = "images/ftv2mlastnode.png";
	  }
	  folder.style.display = 'block';
	}
}

function bookImage(id)
{
	var url = '?'+id.name;

	if ( id.name != '*' ){ window.location.href = url ; }
}

function bookClick(id, sName, s_cd, sImageId, sNotes, sImgCnt, sBookId, obj)
{
	var c_txt = document.getElementById(id);
	var title = document.getElementById(id+'0');
	var notes = document.getElementById(id+'2');
	var stats = document.getElementById(id+'3');
	var image = document.getElementById(id+'img');

	title.innerHTML = sName;
	notes.innerHTML = sNotes;
	stats.innerHTML = 'Date: '+s_cd+' ~ '+sImgCnt+' Images';
	image.name = sBookId;
	
	if ( sImageId.substring(sImageId.indexOf("bid=")) == 'bid=0' ){
		image.src = 'images/iris_40.png';
	} else {
		image.src = 'media.php?'+sImageId;
	}

	if ( c_txt.style.display == 'none' ) {
		c_txt.style.display = 'block';
	}
}


