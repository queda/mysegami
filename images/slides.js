// $ID:$

var img_timer = 0;
var img_delay = 5;
var img_cdown = 0;
var nxt_url = '';

function set_delay( inForm )
{
	for (var i=0; i< inForm.f_delay.length; i++ ) {
		if ( inForm.f_delay[i].selected == true ) {
			img_delay = inForm.f_delay[i].value;
		}
	}

	if (img_delay == 0 ) {
		clearTimeout( img_timer ) ;
	}
	else {
		img_cdown = img_delay;
		img_timer = setTimeout( "countdown()", 1000 ) ;
	}

	var id = document.getElementById( 't_left' );
	id.innerHTML = img_cdown;
//	alert("delay: "+img_delay);
}

function countdown()
{
	--img_cdown;

	var id = document.getElementById( 't_left' );
	id.innerHTML = img_cdown;

	if ( img_cdown == 0 ){
		next_image();
		return false;
	}

	img_timer = setTimeout( "countdown()", 1000 ) ;
}

function slide_setup( url )
{
	nxt_url = url;
	if ( nxt_url == "#" ) { return false; }
	var form = document.forms['delay'];
	set_delay( form );
}

function next_image()
{
//	alert("next_image:"+nxt_url);

	var url = nxt_url;
	url += "&dly="+img_delay;

	location.href = '?'+url;
}

