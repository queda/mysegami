#! /bin/sh

DB='photolog'
LST='book book_image image image_note image_view site site_image user'

mysqldump -Bed $DB > db-$DB.sql

for i in $LST; do
    mysqldump $DB -ed $i > $i.sql
done

mysqldump -B $DB > db-data.sql

