<?php # $Id: index.php,v 1.3 2004-04-23 03:12:37 paulmcav Exp $

	require 'configs/setup.php';
	require 'libs/Smarty.class.php';
	require 'inc/_common.php';

	// --- start of page processing

	session_register('session');
	$session = &$_SESSION['session'];
	
	// -- base64 decode any QUERY_STRING args
	if ( ($qrystr = $_SERVER['QUERY_STRING']) != '' ) {
		$de64 = base64_decode( $qrystr );
		if ( $de64!='' ) { parse_str($de64,$_GET); } else { $de64 = $qrystr; }
		$_REQUEST = array_merge($_GET,$_REQUEST);
	}
	
	/// -- find page request, if not explicit, maybe implicit via AliasMatch?
	$_REQUEST['page'] = PageRequest( ALIAS_URL,$_REQUEST['page'],"templates/" );
	$session['requri'] = $_SERVER['SCRIPT_URL']."?".enc64($de64);
	
	// -- get a page request so we can start our request (obj::smarty)
	if ( ! $_REQUEST['page'] ) { $_REQUEST['page'] = 'Index'; }
	$obj = CreateObject( ".", $_REQUEST['page'] );

	// -- some smarty settings
	if ( FILTER_A ) { $obj->register_outputfilter( "filter_href_fix" ); }
	$obj->compile_check = true;
	$obj->force_compile = true;
//	$obj->debugging = true;

	// -- ValidateUser, LocateSite, DB connect
	PageSetup( "site.cfg", &$obj );
	// -- process the page, and dump any output
	$obj->main();

// -- eof --
