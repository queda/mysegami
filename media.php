<?php // $Id: media.php,v 1.12 2004-07-27 23:32:12 paulmcav Exp $

// ARGS:

// g    generate images only, don't passthru
// dbid	database image id
// s    scale of image to display ( t,0-5 )

require 'configs/setup.php';
require 'libs/Smarty.class.php';
require 'inc/_common.php';

IncludeObject('.','db_mysql');

# $_SERVER['HTTP_IF_MODIFIED_SINCE']
# $_SERVER['If_NONE_MATCH']
# header( "Etag: 10101-10101-10101" )

#$_DEBUG = 1;

/* setup stuff */
$oldcache = session_cache_limiter( 'public' );

session_register( 'session' );
$session = &$_SESSION['session'];

/** Get info about media from passed record ID
*
*/
function db_media_get( $db, $img_id )
{
	global $session;

	$user_id = $session['userid'];
	
	$sql = "SELECT i.*,u.email,DATE_FORMAT(i.cd,'%Y') AS year FROM image i"
		." LEFT JOIN user u on i.user_id=u.id"
		." WHERE i.id=$img_id AND (status='a' "
		.($user_id!='' ? "OR user_id=$user_id" : "")
		.")";
	$db->query( $sql );

	if ( $db->next_record() ) { return $db->Record; }
}

/** update view count for db item
*
*/
function db_view_update( $db, $img_uid, $img_id, $rez )
{
	global $session;
	global $_SERVER;

	$user_id = $session['userid'];
	
	if ( $user_id && $user_id != $img_uid ) {
		$sql = "UPDATE image SET views=views+1 WHERE id=$img_id"
				." AND user_id!=$user_id";
		$db->query( $sql );

		$sql = "INSERT INTO image_view SET"
				." image_id=$img_id,res='$rez x',user_id=$user_id"
				.",remote_addr='".$_SERVER['REMOTE_ADDR']."'";
		$db->query( $sql );
	}
}

/** Create directory heirarchy ala 'mkdir -p'
*
*/
function mkdir_p($target, $mod)
{
	if ( is_dir( $target ) ){ return 1; }
	if (file_exists($target) && !is_dir($target))
		return 0;                                // failure
	if (substr($target, 0, 1) != '/')
		$target = "/".$target;          // prepend a slash
	if (ereg('\.\.', $target)
//		|| ereg("[^a-zA-Z0-9\._\-\/]", $target)
	){
		echo "bad dirname: $target<br>";
		return 0;                                // if illegal dirname
	}
	if (@mkdir($target,$mod))                    // if new dir created,
		return 1;                                // we are successful

	if (mkdir_p(substr($target, 0, (strrpos($target, '/'))),$mod) == 1)
		return (mkdir_p($target,$mod));
	else
		return 0;
}

/**
*
*/
function img_resize( $fin, $fout, $maxwidth, $maxheight=0, $txt='' )
{
	if ( !is_file($fin) ) { return 1; }

	touch( "$fout.l" );

	$src_size = @getimagesize($fin,$iinfo);
	$scale = min($maxwidth/$src_size[0], $maxheight/$src_size[1]);
	if ( !$scale ){ $scale = 1; }

	$dst_w = intval($src_size[0]*$scale);
	$dst_h = intval($src_size[1]*$scale);
	
	if ( $txt!='' ) {
		$color = "#FfDfFf";
		$copy = "-pointsize 12 -fill '$color'"
			." -font helvetica -draw \"text 4,".($dst_h-4)." '$txt'\"";
		$comnt = "-comment \"$txt\"";
	}
	
	$cmd  = "convert";
	$cmd .= " -quality 80"; 
	if ( $scale != 1 ) {
		$cmd .= " -size ".$dst_w."x$dst_h -resize $dst_w"."x$dst_h";
	}
	$cmd .= " -interlace plane";
//	$cmd .= " +profile";
	if ( !$dst_w || $dst_w > 200 ){ $cmd .= " $copy"; }
    $cmd .= " $comnt";
	$cmd .= ' '.escapeshellarg($fin).' '.escapeshellarg($fout);

	exec ( $cmd );
//	echo "cmd: $cmd<br>";
/*	
	// grab file contents
	$fd = @fopen($fin,'r');
	$img_str = fread( $fd,filesize($fin) );
	fclose( $fd );
	
	$im = ImageCreateFromString($img_str);
	
	#$img_w = ImageSX($im);
	#$img_h = ImageSY($im);

	#$scale = $maxwidth / $img_w;
	#$nim_w = intval( $maxwidth );
	#$nim_h = intval( $img_h * $scale );
	
	$nim = imagecreatetruecolor( $nim_w, $nim_h );
//	imageantialias( $nim, true );

	imagecopyresampled( $nim,$im, 0,0,0,0,
	$nim_w, $nim_h, $img_w, $img_h );

// save and cleanup
//	imagejpeg( $nim );
	imagejpeg( $nim, $fout, 80 );
	imagedestroy( $im );
	imagedestroy( $nim );
*/
	unlink( "$fout.l" );
}

/**
*
*/
function image_content_header( $f_dir, $f_image, $ext, $dload )
{
	global $_Mime_List;

	$_stat = stat($f_image);
	$_is_mod = gmdate("D, d M Y H:i:s", $_stat[9])." GMT";

	/* check to see if server already has an image time */
# $_SERVER['HTTP_IF_MODIFIED_SINCE']
# $_SERVER['If_NONE_MATCH']
# header( "Etag: 10101-10101-10101" )

	$_If_Mod = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
	
//	echo "if: $_If_Mod<br>is: $_stat[9]<br>"; die;
	if ( $_If_Mod != '' && strtotime( $_If_Mod ) >= $_stat[9] ) {
		header( "HTTP/1.x 304 Not Modified" ); return 0;
	}
	
	$img_t = exif_imagetype( $f_image );
	if ( $img_t != '' && $img_t < 13 ) {
		$img_mime = image_type_to_mime_type( $img_t );
	}
	else {
		$img_mime = $_Mime_List[$ext];
	}
	Header("Content-type: $img_mime");
	
	header("Content-Length: ".$_stat[7]);
	header("Last-Modified: ".$_is_mod);

	// Disposition: attachment, inline
	if ( $dload ) { $disp = "attachment"; }
//	else{ $disp = "inline"; }
	
	$name = substr( strrchr( $f_image, '/' ), 1);
	if ( $disp != '' )
		header("Content-Disposition: $disp; filename=\"$name\"");

	return 1;
}

// ----- START: code -----

/* BASE64 QUERY_STRING decoding */
if ( ($qrystr = $_SERVER['QUERY_STRING']) != '' ) {
	$de64 = base64_decode( $qrystr );
	if ( $de64!='' ) { parse_str($de64,$_GET); } else { $de64 = $qrystr; }
	$_REQUEST = array_merge($_GET,$_REQUEST);
}

/* load some configuration info */
$smarty = new Smarty;
	
$smarty->config_load( 'site.cfg','db' );
$config = $smarty->get_config_vars();

/* connect to DB */
$db = new db();
$db->connect( $config['database'],$config['host'],$config['user'],$config['password']);

/* media constants */
#$_Image_Dir = "/data/photos";
#$_Image_Rez = array(0,1024,800,534,400,300,'t','i'=>85,'p'=>85,'v'=>160);
#$_jpeg_cmpv = array(0,55  ,60 ,65 , 75 );
$_Mime_List = array( '' => 'application/octet-stream',
					'avi' => 'video/x-msvideo',
					'mpg' => 'video/mpeg',
					'mov' => 'video/quicktime',
					);

$_Image_Dir = $config['img_dir'];
$_Image_Dat = $config['dat_dir'];
$_Image_Rez = cfg_to_array( $config, 'size' );

/* gather args  */
$_G    = $_REQUEST['g'];		// generate image only
$_DBID = $_REQUEST['dbid'];		// database image id
$_S    = $_REQUEST['s'];		// image size selection
$_F    = $_REQUEST['f'];		// image file name
$_DL   = $_REQUEST['dl'];		// image dowload flag

// --- START: main() ---

// force view size to be thumbnail if user has not logged in
if ( !$session['userid'] && $_S != 5 ) { $_S = 't'; }

#echo "- _G: $_G<br>";
#echo "- _DBID: $_DBID<br>" ;
#echo "- _S: $_S\n" ;

/* check to show a thumbnail of the image */
if ( $_S == 't' ) { $_thumb = 1; } else { $_thumb = 0; }

if ( $_S >= sizeof($_Image_Rez)-2 || $_S=='' ) $_S=sizeof($_Image_Rez)-3;

$view_update = 0;

/* db image id was passed */
if ( $_DBID!='' ) {
	$row = db_media_get( $db, $_DBID );

//	print_r( $row  ); die;

	$_IMG_UID = $row['user_id'];
	$_IMG_P   = $row['dir'];
	$_IMG_N   = $row['name'];
	$_IMG_T   = $row['media'];

	$_IMG_PATH = $_IMG_UID.$_IMG_P;
	#echo "<pre>"; print_r( $row ); echo "</pre>";

	$_IMG_TXT = chr(169).' '.$row['year'].' '.$row['email'];

	// if video clip, try and find mpeg unless S=(t|0)
	if ( $_IMG_T == 'v' && $_S >0 ) {
		$name = substr($_IMG_N,0,-3);
		$mpeg = "$_Image_Dir/$_Image_Dat/$_IMG_PATH/$name"."mpg";
		if ( file_exists( $mpeg ) ){
			$_IMG_PATH = "$_Image_Dat/$_IMG_PATH";
			$_IMG_N = $name."mpg";
		}
	}
	
	$view_update = $row['user_id'];
}
/* breakout image name parts */
else {
	$ext = substr( $_F, -4 );
	// thumbnail of avi, use '.thm' file
	if ( $_S == 't' && !strcasecmp( $ext, ".avi" ) ) {
		$ext = '.thm';
		$_F = substr( $_F,0,-4 ).$ext;
	}
	// strip of png if file is supposed to be a thumbnail
	$_IMG_T = 'i';
	if ( $ext == ".png" ) {
		$_F = substr( $_F,0,-4 );
		$_S = "t";

		if ( (substr( $_F, 0,4 ) == "dat/") ) {
			$_F = substr( $_F, 4 );
		}
	}
	if ( $ext == ".thm" ) {
		#		$_F = substr( $_F,0,-4 ).".avi";
		$_S = "t";

		if ( (substr( $_F, 0,4 ) == "dat/") ) {
			$_F = substr( $_F, 4 );
		}
		$_IMG_T = 'v';
	}

	$_IMG_P = dirname( $_F );
	$_IMG_N = basename( $_F );

	$_IMG_PATH = $_IMG_P;
}

$_IMG_X = substr($_IMG_N,-3);
$_IMG_N = substr($_IMG_N,0,-3);

$_SRC_IMG = $_Image_Dir.'/'.$_IMG_PATH.'/'.$_IMG_N.$_IMG_X;
$_DST_PTH = $_Image_Dir."/$_Image_Dat/".$_IMG_PATH;
	
/* if displaying a thumbnail or not, vary depending on media type */
if ( $_thumb ) {
	if ( $_IMG_T == 'v' ) {		/* video */
		$_DST_IMG = $_Image_Dir.'/'.$_IMG_PATH.'/'.$_IMG_N."thm";
		// try alternate case thumb extension
		if ( !file_exists( $_DST_IMG ) ){
			$_DST_IMG = $_Image_Dir.'/'.$_IMG_PATH.'/'.$_IMG_N."THM";
		}
		if ( !file_exists( $_DST_IMG ) ){
			echo "missing file: $_DST_IMG<br>";
			// try and generate the video clip thumbnail
			
		}
	} else {
		$_DST_IMG = $_DST_PTH."/$_IMG_N$_IMG_X.png";
	}
}
else {
	if ( $_IMG_T == 'v' ) {		/* video */
		$_DST_IMG = $_SRC_IMG;
	} else {
		$_DST_IMG = $_DST_PTH."/$_S-$_IMG_N$_IMG_X";
	}
}

// calc image_xy size, check for 0 size if not a literal '0'
if ( $_S != "0" && !($img_xy=$_Image_Rez[$_S]) ) {
	$img_xy = $_Image_Rez[$_IMG_T];
}

if ( $_DEBUG || 0) {
echo "_v_update: $view_update<br>\n";
echo "_thumb: $_thumb<br>\n";
echo "_S: $_S<br>\n";
echo "_IMG_T: $_IMG_T<br>\n";
echo "ssiz: ".$_Image_Rez[$_S]."<br>\n";
echo "img_xy: ".$img_xy."<br>\n";
echo "S_IMG: $_SRC_IMG<br>\n";
echo "D_IMG: $_DST_IMG<br>\n";
}

// problem with source image
if ( is_dir( $_SRC_IMG) || !file_exists( $_SRC_IMG ) ) {
	Header( "Location: /images/unknown.gif" ); die;
}

// check if source image is newer than dest (updated?)
// don't try and unlink vids
if ( $_IMG_T!='v' ) {
	$s_stat = @stat( $_SRC_IMG );
	$d_stat = @stat( $_DST_IMG );

	//echo "s,d:".($s_stat[10]-$d_stat[10])."<br>";
	if ( $d_stat && ($s_stat[10] > $d_stat[10]) ) {
		@unlink( $_DST_IMG );
	}
}

/* if file exists, output it, else resize */
if ( !file_exists( $_DST_IMG ) && !file_exists( "$_DST_IMG.l" ) ) {
	/* want to create path, and generate DST_IMG */
	mkdir_p( $_DST_PTH, 0775 );

	img_resize( $_SRC_IMG, $_DST_IMG, $img_xy, $img_xy, $_IMG_TXT );
	
	/* told only to generate image, exit */
	if ( $_G != '' ) { die; }
}
else {
	/* told only to generate image, but has already been done, exit */
	if ( $_G != '' ) { die; }
	
	/* some process is generating our output image, wait a while */
	while ( file_exists( "$_DST_IMG.l" ) ) {
		sleep(1);
	}
}

if ( !$_thumb && $view_update ) {
   	db_view_update( $db, $view_update, $_DBID, $img_xy ); }

/* dump image information out, if image is cached, die */

if ( !image_content_header( $_Image_Dir, $_DST_IMG, $_IMG_X, $_DL ) ) {
	die;
}

if ( !$_DEBUG ) { readfile( $_DST_IMG ); die; }

echo "\n\n-readfile( $_DST_IMG )<br>";

#echo "<pre>"; print_r( $_SERVER ); echo "</pre>";


// --- END: main() ---
