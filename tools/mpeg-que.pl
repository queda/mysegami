#! /usr/bin/perl

# $Id: mpeg-que.pl,v 1.1 2004-05-25 05:20:45 paulmcav Exp $

# chdir to BASE
$BASE = "/data/photos";

# queue directory
$MQUE = "mpegq";

# some other info
$HOST = `hostname`; chop $HOST;
$DATE = `date`; chop $DATE;

# log file
$LOG  = "/tmp/mpegq.log";

# conversion script
$CONV = $BASE."/mpeg-conv.sh";

$NICE = 10;

sub proc_job()
{
	$job = $_[0];

	# test job is still there
	if ( ! -s $job ) { return; }

	$job =~ m/(.*)\/(.*)\.job/;

	$path = $1;
	$file = $2;

	$work = "$path/work/$$-$file.job";

	rename $job, $work;

	# ensure job is still active
	if ( ! -s $work ) { return; }
	
	# Job file format
	# <input file>
	# (thumb == generate a thumbnail image)
	
	open FH, $work;
	@data = <FH>;
	close FH;
	chop @data;
	
	open FH, ">$LOG";
	print FH "Date: $DATE\n Job: $work\nData: $data[0]\n";

	system "nice -n $NICE $CONV -m $data[0] dat/ >>$LOG 2>&1";

	# generate a thumbnail image?
	if ( $data[1] eq "thumb" ) {
		system "nice -n $NICE $CONV -t $data[0]";
		print FH "Thumbnail\n";
	}
	close FH;

	# if 'done' dir exists, move jobs there, else unlink
	if ( -d "$path/done" ){
		rename $work, "$path/done/$file.job";
	}
	else {
		unlink $work;
	}
	
	return;
}

# --- MAIN() ----

# need to move to root of photo path
chdir( $BASE );

# already working on this machine
if ( -e "$MQUE/$HOST.work" ){ die ; }

open WF, ">$MQUE/$HOST.work";
print WF "$$\n";
close WF;
	

# iterate through job lists
do {
	# pick up a list of all jobs avail
	@list = glob( "$MQUE/*.job" );
	$i = shift( @list );
	
	# missing work file stop working!
	if ( ! -e "$MQUE/$HOST.work" ){ die ; }

	open WF, ">>$MQUE/$HOST.work";
	print WF "$i\n";
	close WF;

	&proc_job( $i );
}
while ( $#list >= 0 );

unlink "$MQUE/$HOST.work";

