#! /bin/sh

# $ID:$
#
### convert to an mpeg file

LOG="/tmp/lav.log"

mpeg_conv()
{
	IFILE="$1"
	OFILE="$4$2/$3.mpg"
	
	MP2="/tmp/$$-ldat.mp2"
#	AVI="/tmp/$$-ldat.avi"
	M1V="/tmp/$$-ldat.m1v"

	QT="-v 0"
	
	A_MP2="-b 32 -r 32000 -m $QT"
	A_MEN="-ofps 24 -nosound -ovc copy"
#	A_MPG="-B 35 -f 0 -4 2 -2 1 -q 10 -Q 0.5 -b 320 -a 1"
	A_MPG="-B 35 -f 0 -g 6 -G 21 -4 2 -2 1 -q 20 -b 1000 -a 1 $QT"

#	YU_FILT="yuvfps -r 24:1 | yuvdenoise | yuvmedianfilter"
	
	YUFP="yuvfps -r 24:1 -v 0"
	YUDN="yuvdenoise"
	YUMF="yuvmedianfilter"
	
	lav2wav -I +p $IFILE | mp2enc $A_MP2 -o $MP2 
	lav2yuv +p $IFILE| $YUFP| $YUDN| mpeg2enc $A_MPG -o $M1V
	if [ -e $MP2 ]; then
		mplex -v 0 -V -r 360 $MP2 $M1V -o $OFILE
	else
		mplex -v 0 -V -r 360 $M1V -o $OFILE
	fi
	
	chown apache $OFILE

	rm -f $MP2
	rm -f $AVI
	rm -f $M1V
}

### grab a frame from the movie

lav_grab_frame()
{
	IFILE="$1"
	OPATH="$2"
#	OPATH="$4$2"
	OFILE="$OPATH/$3.thm"
	
	F=`basename "$IFILE"`
	
	PNM="/tmp/$$-thumb.pnm"
	
	YUDN="yuvdenoise"
	YUMF="yuvmedianfilter"
	Y2PM="y4mtoppm"
	POUT="pnmsplit"

#	START="-o0"

	lav2yuv +p $START -f1 $IFILE| $YUMF| $YUDN| $Y2PM| $POUT - ${PNM}%d
 	pnmscale -xysize 160 120 ${PNM}0| pnmtojpeg -> $OFILE

	chown apache $OFILE

	rm -f ${PNM}0
}

mplayer_grab_frame()
{
	IFILE="$1"
	OPATH="$4$2"
	OFILE="$OPATH/$3.thm"
	
	F=`basename "$IFILE"`
#	OARG="-vo png -z 5"
	OARG="-vo jpeg -jpeg outdir=$OPATH"
	ARGS="-nosound -vop scale=160:120 -frames 1 $OARG"
	
	mplayer $ARGS -ss 1 $1
	
	mv $OPATH/00000001.jpg $OFILE
#	mv 00000001.png $2/$3.png

	rm $OPATH/00000002.*
}

FN=`basename "$2" .avi`
FN=`basename "$FN" .AVI`
FN=`basename "$FN" .mov`
FN=`basename "$FN" .MOV`
DN=`dirname "$2"`

args()
{
	echo "$0 -m | -t video_clip.avi"
	echo " -m .. convert video to mpeg"
	echo " -t .. grab a thumbnail png image from video"
}

# $1 == flag
# $2 == input filename
# $3 == output prefix (optional)
# DN == dirname
# FN == filename

case "$1" in
	"-m" )
	mpeg_conv $2 $DN $FN $3
	exit
	;;
	"-t" )
	lav_grab_frame $2 $DN $FN $3
	exit
	;;
	* )
	args
	;;
esac

